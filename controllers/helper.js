/**
 * Helper for maintaining returnJson structure in one place
 */
function resMsg(req, res, status, data, errors) {
    const len = data == null ? 0 : data.toString().length

    if (req.get('origin') === 'http://localhost:4200')
        res.header({
            'Access-Control-Allow-Origin': 'http://localhost:4200',
            'Content-Length': len
        })

    res.header('Content-Type', 'application/json')
        .status(status)
    return {data, errors}
}

exports.resMsg  = resMsg
