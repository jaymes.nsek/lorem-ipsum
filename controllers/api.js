require('../db') // Call to init correct db
const {body, validationResult} = require('express-validator')
const Product = require('../models/product')
const {resMsg} = require("./helper");
const ApiM = require("../apimetrics");
const {STATUS_CREATED, STATUS_BAD_REQUEST, STATUS_OK, STATUS_PRECONDITION_FAILED, STATUS_NO_CONTENT} = require("../db/statuscode");

/**
 * Create the product
 */
exports.create = [
    // Validate and Sanitize fields.
    name(), description(), price(), size(), origin(),

    // Process request depending on validation and sanitisation results.
    function (req, res) {
        const errors = validationResult(req)

        if (!errors.isEmpty())
            // Return code 400 and the first err (which is the cause).
            return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, errors.array()[0]))

        const rb = req.body
        const product = new Product({
            name: rb.name, description: rb.description, price: rb.price, size: rb.size, origin: rb.origin
        })

        product.save((err, doc) => {
            if (err)
                // Likely Duplicate error with err.code === 11000,
                // or Validation error with err.name === 'ValidationError'
                return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, err.message))
            res.json(resMsg(req, res, STATUS_CREATED, doc, null))
        })
    }
]

/**
 * Return the product, in db, with the associated route param-id
 *
 * @req.body.data the read Model
 */
exports.read = function (req, res) {
    Product.findById(
        req.params.id,
        function (err, doc) {
            if (err)
                return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, err.message))
            res.json(resMsg(req, res, STATUS_OK, doc, null))
        })
}

/**
 * Return all products in db
 */
exports.readAll = function (req, res) {
    Product.find().exec(function (err, docs) {
        if (err)
            return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, err.message))
        res.json(resMsg(req, res, STATUS_OK, docs, null))
    })
}

/**
 * Update the given params within an existing model.
 *
 * @req.body.data the affected Model _id, or null to denote a failed update request.
 */
exports.update = [
    // All params are optional, however, if provided, they must pass validation and sanitisation
    name(true), description(), price(true), size(), origin(),

    // Process request depending on validation and sanitisation results.
    function (req, res) {
        const errors = validationResult(req)

        if (!errors.isEmpty())
            // Return code 400 and the first err (which is the cause).
            return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, errors.array()[0]))

        const name = req.body.name
        const description = req.body.description
        const price = req.body.price
        const size = req.body.size
        const origin = req.body.origin

        // No update-values have been passed
        if (name === undefined && description === undefined && price === undefined &&
            size === undefined && origin === undefined)
            return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, 'No update-values were passed!'))

        const updateObj = {}
        if (name !== undefined)
            updateObj.name = name
        if (description !== undefined)
            updateObj.description = description
        if (price !== undefined)
            updateObj.price = price
        if (size !== undefined)
            updateObj.size = size
        if (origin !== undefined)
            updateObj.origin = origin

        Product.findByIdAndUpdate(
            req.params.id,
            updateObj,
            //options new - returns modified version; - runValidators: validates update op against Product schema
            {new: true, runValidators: true},
            function (err, doc) {
                if (err)
                    // Example err.message for ValidationError is:
                    // "Validation failed: origin: `Canada` is not a valid enum value for path `origin`."
                    return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null, err.message))
                res.json(resMsg(req, res, STATUS_CREATED, doc, null))
            })
    }
]

/**
 * Delete the {@link Product}, in db, with the associated route param-id
 *
 * @req.body.data the number of rows affect, either 0 to denote failure, or 1 for successful deletion.
 */
exports.delete = function (req, res) {
    Product.findByIdAndDelete(
        req.params.id,
        function (err, doc) {
            if (err)
                return res.json(resMsg(req, res, STATUS_BAD_REQUEST, null,err.message))
            res.json(resMsg(req, res, STATUS_OK, doc._id, null))
        })
}

//region POST-UPDATE validator helpers to adhere to DRY principle

function name(optional = false) {
    let validationChain = optional ? body('name').optional({checkFalsy: true}) : body('name');
    const min = ApiM.NAME_MIN
    return validationChain.trim()
        .isLength({min}).withMessage(`name should contain at least ${min} characters.`)
        .escape().matches(ApiM.ALPHA_NUM_SYM_RE) //.isAlpha('en-GB', {ignore: ' *-+/.'})
        .withMessage('name should contain only alphabetic characters and whitespaces.')
}

function price(optional = false) {
    let validationChain = optional ? body('price').optional({checkFalsy: true}) : body('price');
    return validationChain.trim()
        .escape().toFloat().isNumeric()
        .withMessage('price, whether String or Number, should be numeric.')
}

// description, size and origin are always optional, so no need for flag
function description() {
    const min = ApiM.DESC_MIN
    return body('description')
        .optional({checkFalsy: true}).trim()
        .isLength({min}).withMessage(`description should contain at least ${min} characters.`)
        .escape()
        .matches(ApiM.ALPHA_NUM_SYM_RE) //.isAlpha('en-GB', {ignore: ' *-+/.'})
        .withMessage("description should contain only alphabetic, '.' or whitespaces characters.")
}

function size() {
    return body('size', "size should contain alphanumerics, whitespace and/or  '*-+/.' characters")
        .optional({checkFalsy: true}).trim()
        .escape().matches(ApiM.ALPHA_NUM_SYM_RE)
    //.isAlphanumeric('en-GB', {ignore: ' *-+/.'})
}

function origin() {
    const optionsStr = ApiM.ORIGIN_ARR.toString().replaceAll(',', ', ')
    return body('origin', `origin should be one of: ${optionsStr}`)
        .optional({checkFalsy: true})
        .escape().trim()
        //.isIn(ApiM.ORIGIN_ARR) is case-sensitive hence #matches
        .matches(ApiM.ORIGIN_RE_OBJ)
}

//endregion

/**
 * To allow testing front and backend on different PORTS. Maybe remove in Production!
 */
exports.corsPreflight_OPTIONS = function (req, res) {
    // Allowed CORS methods and origins
    const validMethods = ['POST', 'PUT', 'GET', 'DELETE', 'OPTIONS']
    const validOrigins = ["http://localhost:4200"]

    //const host = req.get('host')
    const origin = req.get('origin')
    const method = req.get('Access-Control-Request-Method')

    // Populate the universal scope headers
    res.header({
        'date': (new Date()).toUTCString(),
        'Access-Control-Allow-Origin': "http://localhost:4200",
        'Access-Control-Allow-Methods': 'POST, PUT, GET, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': 86400
    })

    // console.log("host=" + host + " origin=" + origin + " method=" + method)
    let msg
    if (!validMethods.includes(method.toUpperCase()) || !validOrigins.includes(origin.toLowerCase())) {
        msg = 'Reason: CORS header \'Access-Control-Allow-Origin\' missing'
        res.header('Content-Length', msg.length)
            .status(STATUS_PRECONDITION_FAILED) //412 Precondition Failed
            .send()
    }

    msg = ''
    res.header('Content-Length', msg.length) // Required
        .status(STATUS_NO_CONTENT) // 204 No Content
        .send(msg)
}
