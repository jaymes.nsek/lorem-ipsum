
## INTRO

The application showcases a fullstack development environment using _**Angular**_ for frontend 
and _**Express**_ for backend. It features, in the frontend, a supermarket inventory table that 
is able to conduct CRUD operations against a backend database. Validation is also provide for both ends.

The application root contains the backend and frontend elements. Whilst backend files are
directly situated from the root folder: "/", frontend files are all contained in the
angular folder: "/angular".

Note that the frontend and backend run on different ports; this being 3000 and 4200, 
respectively, if default values are adopted.

* _The project is configured this so that only the minified angular code need be placed in /public_

Docker is used to simulate a remote MongoDB database client; 

To get up and running follow the instructions provided in the setup section below, and sequentially.

## SETUP

### Base setup

1. After cloning the repository, create a _**.env**_ file at the project root, 
using the  example format provided in _**.env.example**_

### Start Docker and the Database

2. Docker should be installed and running.
3. Navigate to the root dir (_**../lorem-ipsum>**_) on the terminal
4. Invoke _**docker-compose up**_ (this instantiates MongoDB based on the _docker-compose.yml_ specification)

### Start the Backend Server

5. Once MongoDB Client is up and running, in a new terminal, navigate to the 
root dir, start the server with _**npm run serverstart**_

### Serve the Frontend

6. Naviagte to _**../lorem-ipsum/angular>**_ - where the angular files currently live - and enter _**ng serve**_
or _**ng s**_, for shorthand; this should serve the app on localhost:4200

### Navigate to the WebApp

7. Navigate to _**localhost:4200**_ to view and interact with the app

### Populate the Database with test data (optional)
* The DB should be running as per instructions from _**Start the Database**_
* Run _**node populate-db**_ on the root to populate the database with 232 products

## INTERACTING WITH THE BACKEND

As previously stated, the backend is hosted by default on PORT 3000, to interact with it,
use localhost:3000/api/<other_paths>

* All endpoints return JSON

Refer to the route and controller files for available endpoints and their return values.

## TESTS


### Express Test

* _**Ensure that the Docker is up and running when conducting IntegrationTest**_

#### UnitTest

* Invoke _**npm run unittest**_ on the root dir

#### All test (Integrated and Unit tests)

* Invoke _**npm run test**_ on the root dir

### Angular Test

* WIP
