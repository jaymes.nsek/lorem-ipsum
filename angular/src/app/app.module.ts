import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProductTableComponent} from './table/product-table/product-table.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CrudHandlerComponent} from './table/crud-handler/crud-handler.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TableComponent} from './table/table.component';
import {ProductService} from "./global/services/product.service";
import {CRUDPopupComponent} from "./global/modals/crud-popup/crud-popup.component";
import {AppMatModule} from "./app-mat.module";

@NgModule({
  declarations: [
    AppComponent,
    ProductTableComponent,
    CRUDPopupComponent,
    CrudHandlerComponent,
    TableComponent
  ],
  imports: [
    AppMatModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
