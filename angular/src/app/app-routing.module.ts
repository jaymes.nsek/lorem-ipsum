import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductTableComponent} from "./table/product-table/product-table.component";

const routes: Routes = [
  {path: '', component: ProductTableComponent},
  {path: 'products', component: ProductTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
