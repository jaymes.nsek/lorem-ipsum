import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent

  beforeEach(async () => {
    component = new AppComponent()
  });

  it("should create component and have 'Products' as title", () => {
    expect(component).toBeTruthy();
    expect(component.title).toEqual('Products');
  });
});
