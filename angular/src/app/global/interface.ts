export interface Product {
  _id: string
  name: string
  description: string
  price: number
  size: string
  origin: string
  __v: number // TODO Filter this out from Model in backend
}

/**
 * Product signature - minus _id and __v - with all other props, except name and price,
 * set to optional; ensuring that for add, the minimal properties needed to in the
 * backend are provided.
 *
 * @note additionally, using this as type will ensure robust compile-time error notification
 * making input unit-testing redundant.
 */
export interface ProductMin {
  name: string
  description?: string
  price: number
  size?: string
  origin?: string
}

/**
 * Product signature - minus _id and __v - with all props optional; ensuring
 * that for update, the minimal properties, represent change, can be used.
 *
 * @note additionally, using this as type will ensure robust compile-time error notification
 * making input unit-testing redundant.
 */
export interface ProductOpt {
  name?: string
  description?: string
  price?: number
  size?: string
  origin?: string
}

export enum CRUDMethod {
  CREATE,
  READ,
  UPDATE,
  DELETE
}

export interface CRUDRequestData {
  /**
   * Needed to allow null for ADD, since there is no data
   */
  product: Product | null
  method: CRUDMethod
}

export interface CRUDResponseData {
  product: Product | null | undefined
  method: CRUDMethod
}

export type ProductAttr = 'name' | 'description' | 'price' | 'size' | 'origin'

// To differentiate Array from real Object
export type Obj = {
  [key: string]:  string | number | boolean | object
} | Product;

export interface ProductServiceImpl {
  getAll(): Promise<Product[]>

  create(product: Product): Promise<Product | null>

  update(product: object, _id: string | number): Promise<Product | null>

  delete(product: Product): Promise<Product | null>
}
