import {Injectable} from '@angular/core';
import {Product, ProductMin, ProductOpt, ProductServiceImpl} from "../interface";
import {FetchService} from "./fetch.service";
import {isObject, makeProduct, makeProducts} from "../helpers/product-helpers";

export const ERR_MISMATCH = 'Error: ProductService return data does not match Product schema.'
export const ERR_GENERIC = 'Error: ProductService has encountered an unexpected error.'

@Injectable({
  providedIn: 'root'
})
export class ProductService implements ProductServiceImpl {

  constructor(private fetchService: FetchService) {
  }

  getAll(): Promise<Product[]> {
    return this.fetchService.fetch('/products', {method: "GET"})
      .then(result => {
        let products: Product[] | null = Array.isArray(result) ? makeProducts(result) : []

        // If #makeProducts returned "null" due non-conformance to type Product, reject.
        return (!products) ? Promise.reject(ERR_MISMATCH) : products
      }).catch(err => {
        return Promise.reject(err ?? ERR_GENERIC)
      })
  }

  /**
   * @param addObj object type ensures that for update, sending the minimal properties,
   * representing change, is valid.
   */
  create(addObj: ProductMin): Promise<Product | null> {
    return this.fetchService.fetch('/product', {method: 'POST', body: addObj})
      .then(result => {
        return isObject(result) ? makeProduct(result) : null
      }).catch(err => {
        return null
      })
  }

  /**
   * @param updateObj product signature - minus _id and __v - with all props optional;
   * @param _id the database model id
   *
   * @note in addition to sending the minimal object for efficiency, sending unchanged properties
   * that are unique in MongoDB, e.g. Product.name, will cause a DuplicateError.
   */
  update(updateObj: ProductOpt, _id: string | number): Promise<Product | null> {
    // Return immediately, if object has no properties
    if (!updateObj || Object.keys(updateObj).length <= 0)
      return Promise.reject('Update object must contain at least one valid property.');
    return this.fetchService.fetch('/product/' + _id, {method: 'PUT', body: updateObj})
  }

  /**
   * Return (replay) the product arg if the database returns its _id, which signifies
   * that it has been successfully deleted.
   */
  async delete(product: Product): Promise<Product | null> {
    const _id = await this.fetchService.fetch('/product/' + product._id, {method: "DELETE"})
    return (_id && _id === product._id) ? product : null
  }
}
