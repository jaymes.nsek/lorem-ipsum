import { Injectable } from '@angular/core';

const CONTENT_TYPE_JSON = 'application/json';
type Method = 'POST'|'GET'|'PUT'|'DELETE'


@Injectable({
  providedIn: 'root'
})
export class FetchService {

  constructor() { }

  /**
   * @param path the relative path to the backend endpoint, e.g. /api/products
   * @param params
   */
  fetch(path: string, params: {method: Method, body?: object}): Promise<any> {
    return new Promise(resolve => {
      fetch(new Request(
        FetchService.fullPath(path)), FetchService.getInit(params.method, params.body)
      )
        .then(res => res.json())
        .then(json => {
          //console.warn('---------------------- Res = ' + json)
          // As per backend schema, data should be either null or instantiated; and
          // represents Product for Add and Update, Product[] for getAll, and _id for Delete
          return resolve(json.data)
        }).catch(err => Promise.reject(err.message))
    })
  }

  private static fullPath(relative: string) {
    // Use the full qualifying domain in dev, since ports are different
    return (location.host === "localhost:4200" ? 'http://localhost:3000' : '')
      + '/api' + relative
  }

  /**
   * Return RequestInit object matching the query type;
   *
   * @method a string whose default value is 'GET'
   * @body an optional Product object
   * @note if is GET request the body property is omitted - so that error is not thrown.
   */
  private static getInit(method: Method, body?: object) {
    const headers = {
      'Content-Type': CONTENT_TYPE_JSON,
      'Accept': CONTENT_TYPE_JSON,
    }

    if (method === 'GET' || method === 'DELETE')
      return {method, headers}

    const bodyJson = body ? JSON.stringify(body) : ''
    return {method, headers, body: bodyJson}
  }
}
