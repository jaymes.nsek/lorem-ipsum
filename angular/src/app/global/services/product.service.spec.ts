import {fakeAsync, flushMicrotasks, inject, TestBed} from '@angular/core/testing';
import {ERR_GENERIC, ERR_MISMATCH, ProductService} from './product.service';
import {FetchServiceMock} from "../fakes";
import {Product, ProductMin} from "../interface";
import {FetchService} from "./fetch.service";

describe('ProductService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductService, {provide: FetchService, useClass: FetchServiceMock}]
    })
  })

  it('should initialise', fakeAsync(inject(
    [ProductService, FetchService],
    (service: ProductService, mockFetch: FetchServiceMock) => {
      expect(service).toBeTruthy()
      expect(mockFetch).toBeTruthy()
    })))

  describe('getAll():', () => {
    it('should return products return cast-able to Product[]', fakeAsync(inject(
      [ProductService, FetchService],
      (service: ProductService, mockFetch: FetchServiceMock) => {
        spyOn(mockFetch, 'fetch').and.callThrough()

        let products: Product[] = [], error;

        service.getAll()
          .then(result => {
            products = result
          })
          .catch(err => error = err)

        flushMicrotasks()

        const expectedProducts = [
          {
            _id: '1',
            name: 'Jordans Country Crisp',
            description: 'Jordans Country Crisp With Flame Raisins Cereal',
            size: '500g',
            price: 2,
            origin: 'Europe',
            __v: 1
          },
          {
            _id: '2',
            name: 'Weetabix Oatibix Flakes',
            description: 'Golden Oat Flakes Cereal',
            size: '550g',
            price: 3,
            origin: 'South America',
            __v: 1
          }
        ]

        expect(products).toEqual(expectedProducts)
        expect(error).toBeUndefined()
      })))

    it('should return empty array if fetch is NULL', fakeAsync(inject(
      [ProductService, FetchService],
      (service: ProductService, mockFetch: FetchServiceMock) => {
        spyOn(mockFetch, 'fetch').and.resolveTo(null)

        // Set array with dummy data to differentiate initial state from empty return []
        let products: Product[] = [{__v: 0, _id: "", description: "", name: "", origin: "", price: 0, size: ""}]
        let error;

        service.getAll()
          .then(result => {
            products = result
          })
          .catch(err => error = err)

        flushMicrotasks()

        expect(products).toEqual([])
        expect(error).toBeUndefined()
        // @ts-ignore - #toHaveBeenCalledWith incorrectly inverting args
        expect(mockFetch.fetch).toHaveBeenCalledWith('/products', {method: "GET"})
      })))

    describe('result not of type Product[]', () => {
      interface Person {
        name: string
        age: number
      }

      const nonTypeResult: Person[] = [
        {
          name: 'Joe Bloggs',
          age: 24
        },
        {
          name: 'Anne Baker',
          age: 30
        }
      ]

      const testProd: Product = {
        _id: '2',
        name: 'Weetabix Oatibix Flakes',
        description: 'Golden Oat Flakes Cereal',
        size: '550g',
        price: 3,
        origin: 'South America',
        __v: 1
      }

      const mixedArr = [
        testProd,
        {
          name: 'Joe Bloggs',
          age: 24
        }
      ]

      const params = [
        ['should reject result not of Product[] with error', nonTypeResult],
        ['should reject mixed result, even with some Product[], with error', mixedArr]
      ]

      params.forEach(([desc, mockResult]) => {
        it(String(desc), fakeAsync(inject(
          [ProductService, FetchService],
          (service: ProductService, mockFetch: FetchServiceMock) => {
            spyOn(mockFetch, 'fetch').and.resolveTo(mockResult)
            let products, error;

            service.getAll()
              .then(result => {
                products = result
              })
              .catch(err => {
                error = err
              })

            flushMicrotasks()

            // products equal undefined because .then() is never invoked, since rejected short-circuits to .catch()
            expect(products).toBeUndefined()

            // 'ProductService error, return data does not match Product schema.'
            // @ts-ignore
            expect(error).toEqual(ERR_MISMATCH)
            // @ts-ignore - #toHaveBeenCalledWith incorrectly inverting args
            expect(mockFetch.fetch).toHaveBeenCalledWith('/products', {method: "GET"})
          }))
        )
      })
    })

    describe('#catch block triggering', () => {
      const param = (desc: string, expected: string, rejectWith: string | null) => {
        return {desc, expected, rejectWith}
      }

      const customRejectMsg = "Test reject message."
      const params = [
        param('should reject with default err message if err obj is null', ERR_GENERIC, null),
        param('should reject with err.message if present', customRejectMsg, customRejectMsg)
      ]

      params.forEach(({desc, expected, rejectWith}) => {
        it(desc, fakeAsync(inject(
          [ProductService, FetchService],
          (service: ProductService, mockFetch: FetchServiceMock) => {
            // new Error('This is a test reject err msg.')
            spyOn(mockFetch, 'fetch').and.rejectWith(rejectWith)

            // Set array with dummy data to differentiate initial state from empty return []
            let products, error;

            service.getAll()
              .then(result => {
                products = result
              })
              .catch(err => error = err)

            flushMicrotasks()

            expect(products).toBeUndefined()
            // @ts-ignore
            expect(error).toEqual(expected)
            // @ts-ignore - #toHaveBeenCalledWith incorrectly inverting args
            expect(mockFetch.fetch).toHaveBeenCalledWith('/products', {method: "GET"})
          })))
      })
    })

  })

  describe('create():', () => {
    describe('If result is NOT an object', () => {
      const productObj = {
        name: 'Campo Viejo Red Wine',
        description: 'Red Wine',
        size: '525cl',
        price: 7,
        origin: 'Australia',
      }

      const param = (desc: string, resolveTo: any) => ({desc, resolveTo});

      const params = [
        param('should return "null" for an Array', ['Campo Viejo Red Wine', '525cl', 7]),
        param('should return "null" for undefined/null', null)
      ]

      params.forEach(({desc, resolveTo}) => {
        it(desc, fakeAsync(inject(
          [ProductService, FetchService],
          (service: ProductService, mockFetch: FetchServiceMock) => {
            spyOn(mockFetch, 'fetch').and.resolveTo(resolveTo)

            let products, error;

            service.create(productObj)
              .then(result => {
                products = result
              })
              .catch(err => error = err)

            flushMicrotasks()

            expect(products).toBeNull()
            expect(error).toBeUndefined()
            expect(mockFetch.fetch).toHaveBeenCalledWith(
              // @ts-ignore
              '/product',
              {
                method: 'POST', body: {
                  name: 'Campo Viejo Red Wine',
                  description: 'Red Wine',
                  size: '525cl',
                  price: 7,
                  origin: 'Australia',
                }
              })
          })))
      })
    })

    describe('If result is an Object', () => {

      const argProd: ProductMin = Object.freeze({
        name: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
        description: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
        size: '525cl',
        price: 7,
        origin: 'Australia',
      })

      describe('return Product', () => {
        // Use separate object init - for identical copy - to avoid reference soiling test
        const resolveTo1 = {
          _id: '3',
          name: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
          description: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
          size: '525cl',
          price: 7,
          origin: 'Australia',
          __v: 1
        }

        const resolveTo2 = {
          _id: '3',
          name: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
          description: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
          size: '525cl',
          price: 7,
          origin: 'Australia',
          __v: 1,
          isVegan: true,
          manufacturer: 'Sainsbury'
        }

        const expectedProd: Product = Object.freeze(JSON.parse(JSON.stringify(resolveTo1)))

        const params = [
          {desc: 'if contains all, and only, props satisfying Product', argProd, resolveTo: resolveTo1},
          {desc: 'if satisfies Product but contains unsolicited props', argProd, resolveTo: resolveTo2}
        ]

        params.forEach(({desc, argProd, resolveTo}) => {
          it(desc, fakeAsync(inject(
            [ProductService, FetchService],
            (service: ProductService, mockFetch: FetchServiceMock) => {
              spyOn(mockFetch, 'fetch').and.resolveTo(resolveTo)

              let products, error;

              service.create(argProd)
                .then(result => {
                  products = result
                })
                .catch(err => error = err)

              flushMicrotasks()

              // @ts-ignore
              expect(products).toEqual(expectedProd)
              expect(error).toBeUndefined()
              expect(mockFetch.fetch).toHaveBeenCalledWith(
                // @ts-ignore
                '/product',
                {method: 'POST', body: argProd}
              )
            })))
        })
      })

      describe('return NULL if partially satisfied', () => {
        const params = [
          {
            desc: 'containing ONLY "name" and "price"',
            resolveTo: {
              name: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
              price: 7,
            }
          },
          {
            desc: 'with "name" and "price", along with unsolicited props',
            resolveTo: {
              name: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
              price: 7,
              isVegan: true,
              manufacturer: 'Sainsbury'
            }
          }
        ]

        params.forEach(({desc, resolveTo}) => {
          it(desc, fakeAsync(inject([ProductService, FetchService],
            (service: ProductService, mockFetch: FetchServiceMock) => {
              spyOn(mockFetch, 'fetch').and.resolveTo(resolveTo)

              let products, error;

              service.create(argProd)
                .then(result => {
                  products = result
                })
                .catch(err => error = err)

              flushMicrotasks()

              // @ts-ignore
              expect(products).toBeNull()
              expect(error).toBeUndefined()
              expect(mockFetch.fetch).toHaveBeenCalledWith(
                // @ts-ignore
                '/product',
                {method: 'POST', body: argProd}
              )
            })))
        })
      })
    })
  })
})
