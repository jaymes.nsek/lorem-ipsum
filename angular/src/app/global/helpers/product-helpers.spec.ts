import {
  makeProducts, getIfExists, isAddable, makeProduct, ofProductPropType, isObject
} from "./product-helpers";
import {Product} from "../interface";

describe('product-helpers Tests', () => {

  const testObj = {
    name: "Jordan's Country Crisp",
    description: 'Crisp With Flame Raisins Cereal',
    price: 2
  }

  describe('#isObject', () => {
    it(' should return true if of type Object, { }', () => {
      const obj = {
        dic: {a: "apple", b: 'ball'},
        arr: [1, 3, 45, 5, [34, 67]],
        str: "test"
      }

      expect(isObject(obj)).toBeTrue()
    })

    it(' should return false if an Array, [ ]', () => {
      expect(isObject([1, 2, 'two'])).toBeFalse()
    })
  })

  describe('#getIfExists', () => {
    it('should return the value for "name" prop.', () => {
      expect(getIfExists(testObj, 'name'))
        .toEqual("Jordan's Country Crisp")
    })

    it('should return undefined for "size" prop, which does not exist.', () => {
      expect(getIfExists(testObj, 'size')).toBeUndefined()
    })
  })

  describe('#isAddable', () => {
    it('should return true if "name" and "price" are present', () => {
      expect(isAddable(testObj)).toBeTrue()
    });

    it('should return false if "name" and "price" are NOT present', () => {
      expect(isAddable({
        name: "Jordan's Country Crisp",
      })).toBeFalse()
    });
  })

  describe('#ofProductPropType', () => {
    const testProduct = {
      _id: '1',
      name: undefined,
      price: 2.15,
      size: 42.5, // should be typeof "string"
      address: '22 Angular Street'
    }

    it('should return "true and value" for object prop type matching Product prop', () => {
      expect(ofProductPropType(testProduct, 'price')).toEqual({exists: true, value: 2.15})
    })

    it('should return "false and undefined" if object property type diverts from Product', () => {
      expect(ofProductPropType(testProduct, 'size'))
        .toEqual({exists: false, value: undefined})
    })

    it('should return "false and undefined" if given object property does not exist in Product', () => {
      // Does not exist on either Product or Object
      expect(ofProductPropType(testProduct, 'none'))
        .toEqual({exists: false, value: undefined})

      // Exists on Object but not on Product
      expect(ofProductPropType(testProduct, 'address'))
        .toEqual({exists: false, value: undefined})
    })
  })

  describe('#makeProduct and #areProducts tests', () => {
    const testProducts: Product[] = [
      {
        _id: '1',
        name: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
        description: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
        size: '42g',
        price: 2.1,
        origin: 'North America',
        __v: 1
      },
      {
        _id: '2',
        name: 'Nestle Shredded Wheat Bitesize Cereal 720g',
        description: 'Nestle Shredded Wheat Bitesize Cereal 720g',
        size: '38g',
        price: 2.75,
        origin: 'Africa',
        __v: 1
      }
    ]

    describe('#makeProduct', () => {
      it('should return Product if object matches all Product props', () => {
        expect(makeProduct(testProducts[1])).toEqual({
          _id: '2',
          name: 'Nestle Shredded Wheat Bitesize Cereal 720g',
          description: 'Nestle Shredded Wheat Bitesize Cereal 720g',
          size: '38g',
          price: 2.75,
          origin: 'Africa',
          __v: 1
        })
      })

      it('should return "null" if object only partially satisfies Product', () => {
        const invalidObj = {
          _id: '2',
          name: 'Nestle Shredded Wheat Bitesize Cereal 720g',
          description: 'Nestle Shredded Wheat Bitesize Cereal 720g',
        }
        expect(makeProduct(invalidObj)).toBeNull()
      })

      it('should return "null" if all props are present in object but incorrectly typed', () => {
        const invalidObj = {
          _id: '1',
          name: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
          description: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
          size: 42, // should be typeof "string"
          price: '2.1', // should be typeof "number"
          origin: 'North America',
          __v: 1
        }
        expect(makeProduct(invalidObj)).toBeNull()
      })

      it('should return "null" if object contains no Product props', () => {
        const invalidObj = {
          streetName: 'Angular street',
          number: 21,
        }
        expect(makeProduct(invalidObj)).toBeNull()
      })
    })

    describe('#makeProducts', () => {
      it('should return Product[] if object[] satisfies Product[]', () => {
        expect(makeProducts(testProducts)).toEqual([{
          _id: '1',
          name: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
          description: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
          size: '42g',
          price: 2.1,
          origin: 'North America',
          __v: 1
        },
          {
            _id: '2',
            name: 'Nestle Shredded Wheat Bitesize Cereal 720g',
            description: 'Nestle Shredded Wheat Bitesize Cereal 720g',
            size: '38g',
            price: 2.75,
            origin: 'Africa',
            __v: 1
          }])
      })

      it('should return "null" if ANY object in object[] does not satisfies Product', () => {
        let invalidObjArr = [{
          _id: '3',
          name: 'Nestle Shredded Wheat Bitesize Cereal 720g',
          description: 'Nestle Shredded Wheat Bitesize Cereal 720g',
        }]

        // Add valid products to the invalid array
        invalidObjArr.push(...testProducts)
        expect(invalidObjArr).toHaveSize(3)
        // confirm a valid object is present
        expect(invalidObjArr[1]).toEqual({
          _id: '1',
          name: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
          description: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
          // @ts-ignore
          size: '42g',
          price: 2.1,
          origin: 'North America',
          __v: 1
        })

        expect(makeProducts(invalidObjArr)).toBeNull()
      })

      it('should return "null" if object[] has no Product', () => {
        let invalidObjArr = [{
          streetName: 'React street',
          number: 10,
        },
          {
            streetName: 'Angular street',
            number: 21,
          }
        ]
        expect(makeProducts(invalidObjArr)).toBeNull()
      })
    })
  })
})
