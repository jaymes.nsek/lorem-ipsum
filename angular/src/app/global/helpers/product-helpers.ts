/**
 * Return the associated value if the given property exists in the object, else undefined.
 * @param obj the object to test against
 * @param prop the property to check
 */
import {Obj, Product} from "../interface";

export function isObject(o: object): boolean {
  return o.toString() === '[object Object]'
}

/**
 * @returns undefined if value does not exist in obj, or it does but
 * this value is itself undefined.
 */
export function getIfExists(obj: object, prop: string): any {
  if (!obj || !prop)
    return undefined
  return Object.getOwnPropertyDescriptor(obj, prop)?.value
}

/**
 * Returns true if object contains the minimum required fields
 * @param obj an object intended to pass the minimum required fields for a successful Product add
 */
export function isAddable(obj: object): boolean {
  return !!(getIfExists(obj, 'name') && getIfExists(obj, 'price'))
}

const productSchema: Product = {__v: 1, _id: "", description: "", name: "", origin: "", price: 0, size: ""}
const productProps = Object.keys(productSchema)

/**
 * @note ensures that Product can be created from object if all properties are present;
 * such loose guard ensures that if alien props are present, it does condemn the object.
 */
export function makeProduct(obj: Obj): Product | null {
  const product = {}

  for (let i = 0; i < productProps.length; i++) {
    const key = productProps[i]

    const ofProduct = ofProductPropType(obj, key)
    if (!ofProduct.exists)
      return null

    Object.defineProperty(product, productProps[i], {
      value: ofProduct.value,
      writable: true,
      enumerable: true
    })
  }
  return <Product>product
}

/**
 * Returns an array of {@link Product} if all elements of the array arg
 * satisfy its signature, tested via {@link #makeProduct}; else undefined.
 */
export function makeProducts(objs: Obj[]): Product[] | null {
  let flag = true
  const products: Product[] = []

  for (let obj of objs) {
    const product = makeProduct(obj)
    if (!product) {
      flag = false
      break;
    }
    products.push(product)
  }

  return flag ? products : null
}

/**
 * Return an object of {exists, value} to indicate if the given key is defined in both the object
 * and Product, and types match. If it exists, the value is associated to "value" key.
 *
 * @param obj
 * @param objProp should be a key present with the object arg
 * @return {exists, value} so that if a given value exists but is "undefined", it is
 * possible to tell that it indeed exists but is assigned "undefined".
 */
export function ofProductPropType(obj: object, objProp: string) {
  const descriptor1 = Object.getOwnPropertyDescriptor(productSchema, objProp)
  const descriptor2 = Object.getOwnPropertyDescriptor(obj, objProp)

  let exists = false

  if (!descriptor1 || !descriptor2)
    return {exists, value: undefined}

  const objVal = descriptor2.value
  exists = (typeof descriptor1.value) === (typeof objVal)

  return {exists, value: exists ? objVal : undefined}
}
