import {Product, ProductServiceImpl} from "../interface";

// constant simulating DB products; should not be called directly.
export const _stubProducts = [{
  _id: '1',
  name: 'Jordans Country Crisp',
  description: 'Jordans Country Crisp With Flame Raisins Cereal',
  size: '500g',
  price: 2,
  origin: 'Europe',
  __v: 1
},
  {
    _id: '2',
    name: 'Weetabix Oatibix Flakes',
    description: 'Golden Oat Flakes Cereal',
    size: '550g',
    price: 3,
    origin: 'South America',
    __v: 1
  }
]

export const ProductServiceStub: ProductServiceImpl = {
  getAll: async function () {
    // Dereference the returned products so that modifications by caller are not propagated
    return [..._stubProducts]
  },

  create: async function (product: Product): Promise<Product | null> {
    throw new Error('createProduct is not yet impl')
    // return null;
  },

  update: async function (product: object, _id: string | number): Promise<Product | null> {
    throw new Error('updateProduct is not yet impl')
    // return null;
  },

  delete: async function (product: Product): Promise<Product | null> {
    throw new Error('deleteProduct is not yet impl')
    // return null;
  }
}

export class FetchServiceMock {
  fetch(init: RequestInit, path: string): Promise<any> {
    return Promise.resolve([..._stubProducts])
  }
}
