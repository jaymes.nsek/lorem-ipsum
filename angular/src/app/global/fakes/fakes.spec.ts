import {ProductServiceStub, _stubProducts} from "./fakes";

describe('Returns de-referenced products', () => {
  it('should equal return products on deep comparison', async function () {
    const products = await ProductServiceStub.getAll()
    expect(products).toEqual(_stubProducts)
  });

  it('should NOT equal at address level',  async() => {
    const products = await ProductServiceStub.getAll()
    expect(products !== _stubProducts).toBeTrue()
  })
})
