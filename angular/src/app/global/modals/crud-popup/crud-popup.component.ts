import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-crud-popup',
  templateUrl: './crud-popup.component.html',
  styleUrls: ['./crud-popup.component.css']
})
export class CRUDPopupComponent {
  constructor(public dialogRef: MatDialogRef<CRUDPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string) {
  }

  onCloseClicked(): void {
    this.dialogRef.close();
  }
}
