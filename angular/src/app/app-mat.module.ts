import {MatDialogModule} from "@angular/material/dialog";
import {MatIconModule} from "@angular/material/icon";
import {NgModule} from "@angular/core";


@NgModule({
  imports: [
    MatDialogModule,
    MatIconModule,
  ],
  exports: [
    MatDialogModule,
    MatIconModule,
  ]
})
export class AppMatModule { }
