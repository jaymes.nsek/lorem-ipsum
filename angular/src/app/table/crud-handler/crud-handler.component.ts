import {AfterViewInit, Component, EventEmitter, Input, Output, SimpleChanges} from '@angular/core';
import {CRUDMethod, CRUDRequestData, CRUDResponseData, Product, ProductAttr, ProductMin} from "../../global/interface";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import * as ApiM from "../../../../../apimetrics";
import {ProductService} from "../../global/services/product.service";
import {getIfExists} from "../../global/helpers/product-helpers";

@Component({
  selector: 'app-crud-handler',
  templateUrl: './crud-handler.component.html',
  styleUrls: ['./crud-handler.component.css']
})
export class CrudHandlerComponent implements AfterViewInit {
  // Being settable to undefined ensures that its views can be hidden after they are handled, via binding.
  @Input() crudRequest!: CRUDRequestData | undefined

  @Output() requestCompleteEvent = new EventEmitter<CRUDResponseData>()
  // Propagates the height to parent; naturally, 0 indicates hidden and > 0, visible
  @Output() heightEvent = new EventEmitter<Number>()

  isInvalidUpdate = false

  // Match Validators with backend requirements
  formGroup: FormGroup = this.fb.group({
    name: [
      this.crudRequest?.product?.name || '',
      [Validators.required, Validators.minLength(ApiM.NAME_MIN), Validators.pattern(ApiM.ALPHA_NUM_SYM_RE)]
    ],
    description: [
      this.crudRequest?.product?.description || '',
      [Validators.minLength(ApiM.DESC_MIN), Validators.pattern(ApiM.ALPHA_NUM_SYM_RE)]
    ],
    price: [
      this.crudRequest?.product?.price || '',
      [Validators.required, Validators.pattern(ApiM.FLOAT_RE)]
    ],
    size: [
      this.crudRequest?.product?.size || '',
      Validators.pattern(ApiM.ALPHA_NUM_SYM_RE)
    ],
    origin: [
      this.crudRequest?.product?.origin || '',
      Validators.pattern(ApiM.ORIGIN_RE_OBJ) //
    ],
  })

  constructor(private fb: FormBuilder, private productService: ProductService) {
  }

  ngAfterViewInit() {
    // It was observed that merely registering onresize, was sufficient to trigger the UI to redraw,
    // when window is resized. This behaviour is consistent in Chrome, Firefox and Microsoft Edge.
    // -- Redrawing onresize is necessary to compute template's new height and pass this on to subscribers --
    window.addEventListener('resize', () => {
      //console.warn("*************** CrudHandlerComponent#onResize: ", "onResize")
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    // On Update request, repopulate the FormControls
    const change = changes['crudRequest']
    if (change) {
      const isUpdate = () => this.crudRequest?.method === CRUDMethod.UPDATE

      /*console.warn('crudRequest has changed from  -- ', JSON.stringify(change.previousValue), ' ---  to -- ',
        JSON.stringify(change.currentValue))*/

      let prod = this.crudRequest?.product

      // Revert to the different states - between Update and Add clicks
      const attrs: ProductAttr[] = ['name', 'description', 'price', 'size', 'origin'];
      attrs.forEach(attr => {
        const value = (isUpdate() && prod) ? prod[attr] : ''
        this.formGroup.get(attr)?.setValue(value)
      })
    }
  }

  /**
   * Returns strings of array with '|' as separator,
   * e.g.: Europe|South America|Africa|Australia|MiddleEast|North America
   */
  get originOptUnion() {
    return ApiM.ORIGIN_OPT_UNION
  }

  get originArr(): string[] {
    return ApiM.ORIGIN_ARR
  }

  get apiMetrics() {
    return ApiM;
  }

  //region FetchAPI Triggering clicks

  /**
   * Targets Add and Update operations; propagated through ngSubmit and NOT the buttons
   */
  async onSaveClicked() {
    // This should not occur, but used to realise signature of
    // "CRUDMethod" and not "CRUDMethod | undefined", beyond.
    if (this.crudRequest?.method === undefined)
      return

    let dbProduct = null // The current state of the product in backend
    if (this.crudRequest.method === CRUDMethod.CREATE)
      dbProduct = await this.productService.create(this.uiProduct as ProductMin)
    else if (this.crudRequest.method === CRUDMethod.UPDATE &&
      this.crudRequest.product) {
      let reqProduct = this.crudRequest.product; // The product state that came with request

      let updateMap = new Map<string, string | number>();
      const uiProduct = this.uiProduct;

      // Generate a map of changed properties - then to an object for fetchAPI
      (['name', 'description', 'price', 'size', 'origin'] as ProductAttr[]).forEach((prop => {
        // ensure case-insensitivity of the comparison
        const uiValue = getIfExists(uiProduct, prop)
        if (reqProduct[prop].toString().toLowerCase() !== uiValue.toString().toLowerCase())
          updateMap.set(prop, uiValue)
      }))

      // If there is nothing to update after user clicks on "save", return to prevent pointless
      // update req to backend. This triggers and err message in template.
      this.isInvalidUpdate = updateMap.size <= 0
      if (this.isInvalidUpdate)
        return;

      const updateObj = Object.fromEntries(updateMap)
      //console.warn("CRUDMethod.UPDATE changed values = ", JSON.stringify(updateObj))

      dbProduct = await this.productService.update(updateObj, reqProduct['_id'])
    }

    this.onRequestComplete(dbProduct, this.crudRequest?.method)
  }

  async onDeleteClicked() {
    if (!this.crudRequest?.product)
      return;

    // The entire request Product is sent (rather than just the _id) and is replayed on
    // success, because backend only returns  and used as input to toggleDialogEvent.
    const dbProduct = await this.productService.delete(this.crudRequest.product)
    this.onRequestComplete(dbProduct, this.crudRequest?.method)
  }

  //endregion

  onRequestComplete(product: Product | null, method: CRUDMethod) {
    this.requestCompleteEvent.emit({product, method})
  }

  /**
   * Indicate to {@link AppComponent} that requestEvent should be cancelled
   */
  onCancelClicked() {
    this.requestCompleteEvent.emit(undefined)
  }

  /**
   * Extrapolated ui-data; representing {@link Product} without an _id field.
   */
  get uiProduct(): object {
    return this.formGroup.getRawValue()
  }

  get isUpdateOp() {
    return (this.crudRequest?.product !== undefined && this.crudRequest.product !== null)
      && this.crudRequest.method === CRUDMethod.UPDATE
  }

  returnValueIfUpdate(key: ProductAttr) {
    if (!this.isUpdateOp)
      return ''
    return this.getProductAttr(key)
  }

  /**
   * @param key must on of 'name'|'description'|'price'|'size'|'origin'
   */
  getProductAttr(key: ProductAttr) {
    return this.crudRequest?.product === undefined || this.crudRequest.product === null ?
      '' : this.crudRequest.product[key]
  }

  //region FormControl Helpers

  /**
   * Return boolean, true, indicating that the named {@link FormControl} is valid.
   *
   * @param s the name of the {@link FormControl}
   */
  isValid(s: ProductAttr) {
    //console.log(" CrudHandlerComp: isValid for ", s, 'is = ', flag)
    return this.formGroup.controls[s].valid
  }

  /**
   * Return the value associated with the named {@link FormControl}
   *
   * @param s the name of the {@link FormControl}
   */
  fCtrlVal(s: ProductAttr) {
    const value = this.formGroup.controls[s].value
    //console.warn("=========== value =  " + value.length)
    return value.trim()
  }

  /**
   * Flags that ValidationError should be displayed in template.
   *
   * @returns true where input associated to attr param is invalid, but ignore cases
   * where no input has been issued.
   */
  hasValErr(attr: ProductAttr) {
    return !this.isValid(attr) && this.fCtrlVal(attr) != ''
  }

  //endregion

  //region Form Helpers

  /**
   * Called by template's rootView to both, determine if it should be visible
   * and propagate its height to parent component.
   */
  setVisible(crudRequest: CRUDRequestData | undefined) {
    //console.warn("CrudHandlerComponent#setVisible: ", "CALLED")
    const isSet = !!crudRequest // Set flag true if non-null and defined
    this.heightEvent.emit(isSet ? this.height : 0)
    return isSet
  }

  /**
   * Return the height of the rendered component, represented by the root element.
   */
  get height() {
    const rootView = document.getElementById('crud-container')
    return rootView ? rootView.offsetHeight : 0
  }

  //endregion
}
