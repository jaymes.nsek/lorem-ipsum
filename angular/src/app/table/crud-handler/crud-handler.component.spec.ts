import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CrudHandlerComponent } from './crud-handler.component';
import {RouterTestingModule} from "@angular/router/testing";
import {AppMatModule} from "../../app-mat.module";
import {FormBuilder, FormsModule} from "@angular/forms";
import {ProductService} from "../../global/services/product.service";
import {ProductServiceStub} from "../../global/fakes";

describe('CrudHandlerComponent', () => {
  let component: CrudHandlerComponent;
  let fixture: ComponentFixture<CrudHandlerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CrudHandlerComponent
      ],
      imports: [
        RouterTestingModule,
        AppMatModule,
        FormsModule
      ],
      providers: [
        {provide: ProductService, useValue: ProductServiceStub},
        {provide: FormBuilder}
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
