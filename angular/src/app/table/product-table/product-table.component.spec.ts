import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductTableComponent} from './product-table.component';
import {AppMatModule} from "../../app-mat.module";
import {RouterTestingModule} from "@angular/router/testing";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProductService} from "../../global/services/product.service";
import {ProductServiceStub} from "../../global/fakes";
import {CrudHandlerComponent} from "../crud-handler/crud-handler.component";
import {CRUDMethod, CRUDResponseData} from "../../global/interface";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";
import {CRUDPopupComponent} from "../../global/modals/crud-popup/crud-popup.component";


describe('InventoryTableComponent Tests', () => {
  let component: ProductTableComponent
  let fixture: ComponentFixture<ProductTableComponent>
  let rootElement: HTMLElement
  let productsSize = 2

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ProductTableComponent,
        CrudHandlerComponent,
        CRUDPopupComponent
      ],
      imports: [
        RouterTestingModule,
        AppMatModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        {provide: ProductService, useValue: ProductServiceStub},
        {provide: FormBuilder}
      ]
    }).compileComponents();

    await TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [CRUDPopupComponent]
      }
    })
  })

  beforeEach(async () => {
    fixture = await TestBed.createComponent(ProductTableComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    rootElement = fixture.nativeElement as HTMLElement
  })

  describe('getProducts() onInit and render headers', () => {
    it('should create component and have crud-handler height as 0', () => {
      expect(component).toBeTruthy()
      expect(component.crudHandlerHeight).toBe(0)
    });

    it('should have a total of two products', () => {
      // 2 products reflect inventoryServiceStub#getProducts size
      expect(component.products.length).toBe(productsSize)
    });

    describe("Template's table", () => {
      const params = [
        ['should render name column-attribute', 'name', '0'],
        ['should render description column-attribute', 'description', '1'],
        ['should render price column-attribute', 'price', '2'],
        ['should render size column-attribute', 'size', '3'],
        ['should render origin column-attribute', 'origin', '4']
      ]

      let tableAttrRow: HTMLElement | null

      beforeEach(() => {
        tableAttrRow = rootElement.querySelector('tr[id="header-attr-row"]') as HTMLElement
      })

      params.forEach(([desc, attr, index]) => {
        it(desc, () => {
          const nameAttr = tableAttrRow?.children[Number(index)]
          expect(nameAttr?.textContent).toBe(attr)
        });
      })
    })
  })

  describe('Crud invoking buttons, clicking ...', () => {
    it("_add should set CRUDRequestData #product as null and #method as CREATE",
      () => {
        component.onAddClicked()
        fixture.detectChanges()

        let actual = component.request
        expect(actual?.product).toBeNull()
        expect(actual?.method).toBe(CRUDMethod.CREATE)
      })

    it("_update should set CRUDRequestData #product as products[index] and #method as UPDATE",
      () => {
        // The target index corresponds to the second element of inventoryServiceStub; id == 2
        component.onUpdateClicked(1)
        fixture.detectChanges()

        let actual = component.request
        expect(actual?.product).toEqual({
          _id: '2',
          name: 'Weetabix Oatibix Flakes',
          description: 'Golden Oat Flakes Cereal',
          size: '550g',
          price: 3,
          origin: 'South America',
          __v: 1
        })
        expect(actual?.method).toBe(CRUDMethod.UPDATE)
      })

    it("_delete should set CRUDRequestData #product as products[index] and #method as DELETE",
      () => {
        // The target index corresponds to the second element of inventoryServiceStub; id == 2
        component.onDeleteClicked(0)
        fixture.detectChanges()

        let actual = component.request
        expect(actual?.product).toEqual({
          _id: '1',
          name: 'Jordans Country Crisp',
          description: 'Jordans Country Crisp With Flame Raisins Cereal',
          size: '500g',
          price: 2,
          origin: 'Europe',
          __v: 1
        })
        expect(actual?.method).toBe(CRUDMethod.DELETE)
      })
  })

  describe('onRequestComplete()', () => {
    it('CRUDResponseData of undefined, aka Cancel, should not mutate products', function () {
      // 2 products reflect inventoryServiceStub#getProducts size
      expect(component.products.length).toBe(productsSize)

      component.onRequestComplete(undefined)
      fixture.detectChanges()

      // should remain a total of two products
      expect(component.products.length).toBe(productsSize)
    });

    describe('CRUDResponseData #CREATE', () => {
      const responseData: CRUDResponseData = {
        method: CRUDMethod.CREATE,
        product: {
          _id: String(productsSize + 1),
          name: 'Desperados Tequila Lager Beer Bottles',
          description: 'Beer Bottles 3 x 330ml',
          size: '990ml',
          price: 5,
          origin: 'North America',
          __v: 1
        }
      }

      it('should add newly created to products', () => {
        // 2 products reflect inventoryServiceStub#getProducts size
        expect(component.products.length).toBe(productsSize)

        // Simulate successful add request and return of CRUDResponseData (contains newly added object)
        component.onRequestComplete(responseData)
        fixture.detectChanges()

        // The total number of products should increase by 1
        expect(component.products.length).toBe(productsSize + 1)

        expect(component.products[2]).toEqual({
          _id: String(productsSize + 1),
          name: 'Desperados Tequila Lager Beer Bottles',
          description: 'Beer Bottles 3 x 330ml',
          size: '990ml',
          price: 5,
          origin: 'North America',
          __v: 1
        })
      });

      describe('HTML Template: Verify the third row matches newly pushed Product', () => {
        let addedProdRow_tr: HTMLElement

        beforeEach(() => {
          // Simulate successful add request and return of CRUDResponseData (contains newly added object)
          component.onRequestComplete(responseData)
          fixture.detectChanges()

          const tableRows = rootElement.querySelector('tbody')?.children as HTMLCollection
          addedProdRow_tr = tableRows[thirdProd] as HTMLElement
        })

        //*** Single-Card cell ***
        it('should render single-card contained attrs', function () {
          expect(component.products.length).toBe(productsSize + 1);

          // The first <td> represents the single-card containing elements, with two nested <div>.
          //  The first <div> is the .single-card; a <div>, which contains 3x nested <p> with
          //  the second element containing further nesting of <span> holding description and price values
          const singleCardDiv = (addedProdRow_tr?.firstChild as HTMLElement)
            .querySelector('div[class="single-card"]')

          expect(singleCardDiv?.querySelector('p[class="single-card-entry"]:nth-child(1)')
            ?.textContent).toBe('Desperados Tequila Lager Beer Bottles')

          // desc and size <p>
          expect(singleCardDiv?.querySelector('p[class="single-card-entry"]:nth-child(2)')
            ?.textContent).toBe('Beer Bottles 3 x 330ml 990ml')

          expect(singleCardDiv?.querySelector('p[class="single-card-entry"]:nth-child(3)')
            ?.textContent).toBe('€5.00')
        });

        //*** Other cells ***
        // Test <p> for description, price, size and origin; The HTML struct is: tbody.tr.td.p
        // NB: tdIndex starts from 1, since the .single-card which holds  index 0 is being omitted
        const params = [
          ["should render description as 'Beer Bottles 3 x 330ml'", 'Beer Bottles 3 x 330ml', '1'],
          ["should render price as '€5.00'", '€5.00', '2'],
          ["should render size as '990ml'", '990ml', '3'],
          ["should render origin as 'North America'", 'North America', '4']
        ];

        const thirdProd = 2 // The 3rd element

        params.forEach(([desc, expected, tdIndex]) => {
          it(desc, () => {
            expect(component.products.length).toBe(productsSize + 1)

            // .firstChild represents the <p> - which is nested in the <td>
            expect(addedProdRow_tr?.children[Number(tdIndex)].firstChild?.textContent)
              .toEqual(expected)
          })
        })
      })
    })

    describe('CRUDResponseData #UPDATE', () => {
      const responseData: CRUDResponseData = {
        method: CRUDMethod.UPDATE,
        product: {
          _id: '1',
          name: "Jordan's Crisp",
          description: 'Crisp With Flame Raisins Cereal',
          size: '500g',
          price: 2.10,
          origin: 'South America',
          __v: 1
        }
      }

      it('should reflect updated product in products', () => {
        expect(component.products.length).toBe(productsSize)

        expect(component.products.find(product => product._id === '1')).toEqual({
          _id: '1',
          name: 'Jordans Country Crisp',
          description: 'Jordans Country Crisp With Flame Raisins Cereal',
          size: '500g',
          price: 2,
          origin: 'Europe',
          __v: 1
        })

        // Simulate success UPDATE request and return of CRUDResponseData (containing updated object)
        component.onRequestComplete(responseData)
        fixture.detectChanges()

        // The total number of products should remain the same
        expect(component.products.length).toBe(productsSize)

        // The product is updated
        expect(component.products.find(product => product._id === '1')).toEqual({
          _id: '1',
          name: "Jordan's Crisp", // Changed
          description: 'Crisp With Flame Raisins Cereal', // Changed
          size: '500g',
          price: 2.10, // Changed
          origin: 'South America', // Changed
          __v: 1
        })
      });

      it('should cause or throw error if responseData#product is undefined', () => {
        expect(component.products.length).toBe(productsSize)

        component.onRequestComplete({
          method: CRUDMethod.UPDATE,
          product: null
        })
        fixture.detectChanges()

        // The total number of products should remain the same
        expect(component.products.length).toBe(productsSize)
      })

      describe('HTML Template: Verify the first row matches updated Product', () => {
        let updatedProdRow_tr: HTMLElement
        const firstProd = 0 // The 1st element

        beforeEach(() => {
          // Simulate successful add request and return of CRUDResponseData (contains newly added object)
          component.onRequestComplete(responseData)
          fixture.detectChanges()

          const tableRows = rootElement.querySelector('tbody')?.children as HTMLCollection
          updatedProdRow_tr = tableRows[firstProd] as HTMLElement
        })

        //!*** Single-Card cell ***
        it('should render single-card contained attrs', function () {
          expect(component.products.length).toBe(productsSize);

          // The first <td> represents the single-card containing elements, with two nested <div>.
          //  The first <div> is the .single-card; a <div>, which contains 3x nested <p> with
          //  the second element containing further nesting of <span> holding description and price values
          const singleCardDiv = (updatedProdRow_tr?.firstChild as HTMLElement)
            .querySelector('div[class="single-card"]')

          expect(singleCardDiv?.querySelector('p[class="single-card-entry"]:nth-child(1)')
            ?.textContent).toBe("Jordan's Crisp")

          // desc and size <p>
          expect(singleCardDiv?.querySelector('p[class="single-card-entry"]:nth-child(2)')
            ?.textContent).toBe('Crisp With Flame Raisins Cereal 500g')

          expect(singleCardDiv?.querySelector('p[class="single-card-entry"]:nth-child(3)')
            ?.textContent).toBe('€2.10')
        });

        //!*** Other cells ***
        // Test <p> for description, price, size and origin; The HTML struct is: tbody.tr.td.p
        // NB: tdIndex starts from 1, since the .single-card which holds  index 0 is being omitted
        const params = [
          ["should render description as 'Crisp With Flame Raisins Cereal'", "Crisp With Flame Raisins Cereal", '1'],
          ["should render price as '€2.10'", '€2.10', '2'],
          ["should render size as '500g'", '500g', '3'],
          ["should render origin as 'South America'", 'South America', '4']
        ];

        params.forEach(([desc, expected, tdIndex]) => {
          it(desc, () => {
            expect(component.products.length).toBe(productsSize)

            // .firstChild represents the <p> - which is nested in the <td>
            expect(updatedProdRow_tr?.children[Number(tdIndex)].firstChild?.textContent)
              .toEqual(expected)
          })
        })
      })
    })

    describe('CRUDResponseData #DELETE', () => {
      const responseData: CRUDResponseData = {
        method: CRUDMethod.DELETE,
        product: {
          _id: '2',
          name: 'Weetabix Oatibix Flakes',
          description: 'Golden Oat Flakes Cereal 550g',
          size: '550g',
          price: 3,
          origin: 'South America',
          __v: 1
        }
      }

      it('should remove the db-deleted product from products', () => {
        expect(component.products.length).toBe(productsSize)

        expect(component.products.find(product => product._id === '2')).toEqual({
          _id: '2',
          name: 'Weetabix Oatibix Flakes',
          description: 'Golden Oat Flakes Cereal',
          size: '550g',
          price: 3,
          origin: 'South America',
          __v: 1
        })

        // Simulate success DELETE request and return of CRUDResponseData (containing updated object)
        component.onRequestComplete(responseData)
        fixture.detectChanges()

        // The total number of products should reduce by 1
        expect(component.products.length).toBe(productsSize - 1)

        // The product is removed
        expect(component.products.find(product => product._id === '2')).toBeUndefined()
      });

      it('should not throw error if responseData#product is undefined', () => {
        expect(component.products.length).toBe(productsSize)

        component.onRequestComplete({
          method: CRUDMethod.DELETE,
          product: null
        })
        fixture.detectChanges()

        // The total number of products should remain the same
        expect(component.products.length).toBe(productsSize)
      })

      describe('HTML Template: removes the deleted Product', () => {
        let tableRows: HTMLCollection

        beforeEach(() => {
          // Simulate successful add request and return of CRUDResponseData (contains newly added object)
          component.onRequestComplete(responseData)
          fixture.detectChanges()

          tableRows = rootElement.querySelector('tbody')?.children as HTMLCollection
        })

        it('one row exists after Product delete', function () {
          expect(tableRows.length).toBe(1)
        });

        //*** Single-Card cell ***
        it('should render single-cards without the deleted product', function () {
          // Pattern ensures that test scales if more than 1 row is present after deletion
          let prodRow_tr: HTMLElement
          let singleCardDiv: HTMLElement | null

          for (let trIndex = 0; trIndex < tableRows.length; trIndex++) {
            // The .firstChild is the <td> holding the single-card containing elements, with two nested <div>.
            //  The first <div> is the .single-card; a <div>, which contains 3x nested <p> with
            //  the second element containing further nesting of <span> holding description and price values
            prodRow_tr = tableRows[trIndex] as HTMLElement

            // Extract element 0, the first <td>
            singleCardDiv = (prodRow_tr?.firstChild as HTMLElement)
              .querySelector('div[class="single-card"]')

            const name = singleCardDiv?.querySelector(
              'p[class="single-card-entry"]:nth-child(1)')?.textContent

            const descAndSize = singleCardDiv?.querySelector(
              'p[class="single-card-entry"]:nth-child(2)')?.textContent

            const price = singleCardDiv?.querySelector(
              'p[class="single-card-entry"]:nth-child(3)')?.textContent

            // Flags true if all the attr match, indicating it is the deleted product
            let flag = (name === "Weetabix Oatibix Flakes") && (descAndSize === "Golden Oat Flakes Cereal 550g") &&
              (price === '€3.00')

            // Debugging helper to compensate not running each loop as it()
            if (flag)
              console.error("should render single-card contained attrs test: failed on row",
                trIndex + 1, 'with', JSON.stringify({name, descAndSize, price}))

            expect(flag).toBeFalse()
          }
        });

        //!*** Other cells ***
        // Test <p> for description, price, size and origin; The HTML struct is: tbody.tr.td.p
        // NB: tdIndex starts from 1, since the .single-card which holds  index 0 is being omitted

        it('should no longer contain the deleted product', () => {
          let prodRow_tr: HTMLElement
          let tdCells: HTMLCollection

          // Deleted tr attrs
          const delDesc = 'Golden Oat Flakes Cereal'
          const delSize = '550g'
          const delPrice = '€3.00'
          const delOrigin = 'South America'

          for (let trIndex = 0; trIndex < tableRows.length; trIndex++) {
            prodRow_tr = tableRows[trIndex] as HTMLElement
            tdCells = prodRow_tr?.children as HTMLCollection

            // Ignore the firstChild which is the single-card
            // .firstChild is the nested <p> within <td>
            const desc = tdCells[1].firstChild?.textContent
            const price = tdCells[2].firstChild?.textContent
            const size = tdCells[3].firstChild?.textContent
            const origin = tdCells[4].firstChild?.textContent

            const flag = desc === delDesc && price === delPrice &&
              size === delSize && origin === delOrigin

            // Debugging helper to compensate not running each loop as it()
            if (flag)
              console.error("should render single-card contained attrs test: failed on row",
                trIndex + 1, 'with', JSON.stringify({desc, price, size, origin}))

            expect(flag).toBeFalse()
          }
        })
      })
    })
  })
});
