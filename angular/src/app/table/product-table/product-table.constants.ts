import {CRUDMethod, CRUDResponseData} from "../../global/interface";

export function makeMsg(data: CRUDResponseData): string {
  const failed = 'Request failed.'
  const method = data.method
  const product = data.product

  if (!product)
    return failed

  const getMsg = (end: string) => {
    return `${product.name} ${product._id ? 'was successfully' : 'was NOT'} ${end}.`
  }

  switch (method) {
    case CRUDMethod.CREATE:
      return getMsg('added')
    case CRUDMethod.UPDATE:
      return getMsg('updated')
    case CRUDMethod.DELETE:
      return getMsg('deleted')
    default:
      return failed
  }
}
