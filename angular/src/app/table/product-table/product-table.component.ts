import {Component, OnInit} from '@angular/core';
import {CRUDRequestData, CRUDMethod, Product, CRUDResponseData} from "../../global/interface";
import {ProductService} from "../../global/services/product.service";
import {CRUDPopupComponent} from "../../global/modals/crud-popup/crud-popup.component";
import {MatDialog} from "@angular/material/dialog";
import {makeMsg} from "./product-table.constants";

const URL_PARAM_MSG = 'msg';


@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {
  //@Input()
  products: Product[] = []

  request!: CRUDRequestData | undefined
  crudHandlerHeight: Number = 0;


  constructor(public dialog: MatDialog, private productService: ProductService) {
  }

  ngOnInit() {
    this.relaunchDialogFromParams()

    this.getProducts()

    // Subscribe to dialog close event to remove previously added
    // query params when dialog was shown
    this.dialog.afterAllClosed.subscribe(() => {
      this.removeQueryParams()
    })
  }

  getProducts() {
    this.productService.getAll()
      .then(products => {
        this.products = products
        //console.warn("No. of Products =", this.products.length)
      })
  }

  //region On Add, Update and Delete Listeners
  /***Propagate request to  {@link CrudHandlerComponent} to handle, when clicked.**/

  onAddClicked() {
    this.request = {
      product: null,
      method: CRUDMethod.CREATE
    }
  }

  onUpdateClicked(index: number) {
    const product = this.products[index]

    this.request = {
      product: product,
      method: CRUDMethod.UPDATE
    }
  }

  onDeleteClicked(index: number) {
    const product = this.products[index]

    this.request = {
      product: product,
      method: CRUDMethod.DELETE
    }
  }

  //endregion

  /**
   * Set the height of {@link CrudHandlerComponent}, as the top clearance of
   * {@link ProductTableComponent}'s sticky position, so that it is positioned correctly,
   * relative to the former.
   *
   * @param height the current height of {@link ProductTableComponent}, 0 when hidden
   * and greater than 0 when visible.
   */
  onCrudHandlerHeightChange(height: Number) {
    if (this.crudHandlerHeight === height)
      return //console.warn(":::: onCrudVisibilityToggle: returning, same height of", height)

    const tableRoot = (document.getElementById('itc-root') as HTMLElement)
    tableRoot.style.setProperty('--stickyPosTop', height + 'px')
    this.crudHandlerHeight = height
    //console.warn(":::: onCrudVisibilityToggle: setting height to", height)
  }

  //region Dialog Helpers

  /**
   * Use to by {@link CrudHandlerComponent} to communicate to {@link #this}, after interaction
   * with the database, and indicating that HTTP request has completed and that two things should happen:
   * 1. the dataset should be modified to match backend
   * 2. A confirmation popup dialog should be shown
   *
   * @param data Use "undefined" to signify that the request event should be
   * cancelled, i.e. when user presses cancel in {@link CrudHandlerComponent}
   *
   * @note This strategy is adopted primarily for efficiency; the entire database need not be returned
   * after each add, update or delete op. Instead, receipt of a valid _id, or number of rows affected,
   * in {@link ProductService} indicates success and the previous state can merely be recycled.
   */
  onRequestComplete(data: CRUDResponseData | undefined) {
    // console.warn(' ###################### onRequestComplete: ', JSON.stringify(data))
    // Signifies CANCEL button press or a Request - either way reset to  hide the form/crud container.
    this.request = undefined

    // Return if not a CRUD request but cancel
    if (!data)
      return

    if ((data.method === CRUDMethod.CREATE) && data.product) {
      this.products.push(data.product)
    }
    else if (data.method === CRUDMethod.UPDATE && data.product) {
      const _id = data.product._id
      let prodRef: Product | undefined = this.products.find(aProduct => aProduct._id === _id)

      // Update the model, in place, by reference
      if (prodRef) {
        prodRef.name = data.product.name
        prodRef.description = data.product.description
        prodRef.price = data.product.price
        prodRef.size = data.product.size
        prodRef.origin = data.product.origin
      }
    } else if (data.method === CRUDMethod.DELETE && data.product) {
      // Locate index of delete product and delete from array
      const _id = data.product._id
      const index = this.products.findIndex(aProduct => aProduct._id === _id)

      if (index >= 0)
        this.products.splice(index, 1)
    }

    this.openDialogAndAddQueryParams(makeMsg(data))
  }

  openDialogAndAddQueryParams(msg: string) {
    // Returns dialogRef
    this.dialog.open(CRUDPopupComponent, {
      width: '90%',
      maxWidth: '400px',
      data: msg,
    });

    // Add QueryParams to URL. Dialog#afterOpened is not used since it would
    // require assigning msg globally.
    this.addQueryParams(msg)
  }

  /**
   * @note To be called in {@link ngOnInit} so that QueryParams are extrapolated, before
   *   removal from the effects of subscribing to {@link MatDialog#afterAllClosed},
   *   also in {@link ngOnInit}.
   */
  relaunchDialogFromParams() {
    const params = new URLSearchParams(location.search)
    // dialogMsg is null if no such param with the given key exists
    const dialogMsg = params.get(URL_PARAM_MSG)
    //console.error("AppComponent: searchParams = ", dialogMsg)

    // dialogMsg is null if no such param with the given key exists
    if (!dialogMsg)
      return;
    this.openDialogAndAddQueryParams(dialogMsg)
  }

  /**
   * Append QueryParams to url path when Dialog is shown;
   */
  addQueryParams(dialogMsg: string) {
    // For example, if http://localhost/home - then adding "?success=true&product=product_name"
    // results in: http://localhost/home?success=true&product=product_name
    const queryParams = `?${URL_PARAM_MSG}=${dialogMsg}`
    history.replaceState(null, '', queryParams)
  }

  /**
   * Remove QueryParams from the URL, if present, when the dialog is no longer visible.
   */
  removeQueryParams() {
    history.replaceState(null, '', location.pathname)
  }

  //endregion
}
