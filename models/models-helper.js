/**
 * Capitalise each word in the string.
 *
 * @note words are delimited by the presence of in-between white-spaces.
 */
exports.capitalise = function (str) {
    const wordArr = str.trim().split(/[\s]+/)
    const capWordArr = wordArr.map(
        (word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()
    )
    return capWordArr.join(' ')
}