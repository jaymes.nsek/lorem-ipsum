const mongoose = require('mongoose')
const {capitalise} = require("./models-helper");
const {ORIGIN_ARR} = require("../apimetrics");

let Schema = mongoose.Schema

let ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String
    },
    price: {
        type: Number,
        required: true
    },
    size: String,
    origin: {
        type: String,
        set: origin => capitalise(origin),
        enum: ORIGIN_ARR
    }
});

module.exports = mongoose.model('Product', ProductSchema);
