require('dotenv').config() // Should be called first!
const mongoose = require("mongoose")

// Connect MongoDB to the devAndProd or test database
const testDbSwitch = process.env.NODE_ENV === 'test' ? '_test' : '';

(async function init() {
    await mongoose.connect(process.env.DB_URL,
        {
            user: process.env.DB_USERNAME,
            pass: process.env.DB_PASSWORD,
            dbName: process.env.DB_DATABASE + testDbSwitch}
    )
})()

console.log('========= connected! ========= ' + mongoose.connection.name)

module.exports = mongoose
