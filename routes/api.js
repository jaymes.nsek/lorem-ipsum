const express = require('express')
const router = express.Router()
const apiCtrl = require('../controllers/api')

// POST (Create) Product.
router.post('/product', apiCtrl.create)

// GET (Read) a single model
router.get('/product/:id', apiCtrl.read)

// GET (Read) all products
router.get('/products', apiCtrl.readAll)

// Update all products
router.put('/product/:id', apiCtrl.update)

// Delete a single model instance
router.delete('/product/:id', apiCtrl.delete)

// CORS Preflight
router.options('/product', apiCtrl.corsPreflight_OPTIONS)
router.options('/products', apiCtrl.corsPreflight_OPTIONS)
router.options('/product/:id', apiCtrl.corsPreflight_OPTIONS)

module.exports = router
