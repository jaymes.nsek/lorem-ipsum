/**
 * Centralises backend and frontend validation requirements so that changes occur in one place.
 */

//region Validation Specifications

exports.NAME_MIN = 3
exports.DESC_MIN = 5

const _originArr = ['', 'Europe', 'South America', 'Africa', 'Australia', 'Middle East', 'North America']
exports.ORIGIN_ARR = _originArr

//endregion

//region REG_EX

// \s == whitespace
//exports.ALPHA_SYM_RE = /^[A-Za-z'*-+/.\s]*$/
exports.ALPHA_NUM_SYM_RE = /^[A-Za-z0-9'*-+/.\s]*$/
exports.FLOAT_RE = /^[0-9.]*$/

// Transform array to a string of: Europe|South America|Africa|Australia|MiddleEast|North America
const _originOptUnion = _originArr.join('|')

// Grouping, "()",around options is required to accept only one option
const originRegExp = new RegExp(`^(${_originOptUnion})$`, 'i')
exports.ORIGIN_RE_OBJ = originRegExp

//endregion

//region Informational

exports.ORIGIN_OPT_UNION = _originOptUnion

// ValidationError messages
exports.NAME_MSG = '"name" may contain alphabets, numbers, white-space or \
                    "*-+/." - and be at least 3 characters long.'
exports.DESC_MSG = '"description" may contain alphabets, numbers, white-space or \
                    "*-+/." - and be at least 5 characters long.'
exports.PRICE_MSG = '"price", taken as Euros, should be numeric. Decimal point is valid, e.g.: 2.25.'
exports.SIZE_MSG = '"size" may contain alphabets, numbers, white-space or "*-+/."'
exports.ORIGIN_MSG = '"origin" should be one of: ' + _originOptUnion

//endregion
