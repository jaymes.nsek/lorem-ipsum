const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const apiRouter = require('./routes/api')
const {resMsg} = require("./controllers/helper");


const app = express()
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/api', apiRouter)

// Default error handler - last resort for unhandled middle err when next() is invoked
app.use(function (req, res) {
        res.json(resMsg(req, res, 404, null,
            "Cannot access route/resource, likely because it does not exist."))
});

module.exports = app
