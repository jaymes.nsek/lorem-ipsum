const mongoose = require('./db')
const Product = require('./models/product')

products = [
    {
        name: 'Heineken 0.0 Alcohol Free Beer 12x330ml',
        description: 'Heineken 0.0 Alcohol Free Beer 12x330ml',
        size: '1.77ltr',
        price: 7,
        origin: 'South America'
    },
    {
        name: 'Stella Artois Premium Lager Beer Cans 4x568ml',
        description: 'Stella Artois Premium Lager Beer Cans 4x568ml',
        size: '2.42ltr',
        price: 5.5,
        origin: 'Europe'
    },
    {
        name: 'Hobgoblin Ruby Ale Beer Bottle 500ml',
        description: 'Hobgoblin Ruby Ale Beer Bottle 500ml',
        size: '2.8ltr',
        price: 1.4,
        origin: 'Middle East'
    },
    {
        name: 'San Miguel Premium Lager Beer Cans 4x440ml',
        description: 'San Miguel Premium Lager Beer Cans 4x440ml',
        size: '2.27ltr',
        price: 4,
        origin: 'North America'
    },
    {
        name: 'Wainwright Golden Ale Beer Bottle 500ml',
        description: 'Wainwright Golden Ale Beer Bottle 500ml',
        size: '3ltr',
        price: 1.5,
        origin: 'Middle East'
    },
    {
        name: 'Guinness Draught Stout Beer Can 15x440ml',
        description: 'Guinness Draught Stout Beer Can 15x440ml',
        size: '2.42ltr',
        price: 16,
        origin: 'North America'
    },
    {
        name: 'Heineken Premium Lager Beer Cans 15x440ml',
        description: 'Heineken Premium Lager Beer Cans 15x440ml',
        size: '1.82ltr',
        price: 12,
        origin: 'Middle East'
    },
    {
        name: 'Crabbies Original Alcoholic Ginger Beer 500ml',
        description: 'Crabbies Original Alcoholic Ginger Beer 500ml',
        size: '3.4ltr',
        price: 1.7,
        origin: 'Africa'
    },
    {
        name: 'Birra Moretti Lager Beer Bottles 4 x 330ml',
        description: 'Birra Moretti Lager Beer Bottles 4 x 330ml',
        size: '3.79ltr',
        price: 5,
        origin: 'Australia'
    },
    {
        name: 'San Miguel Alcohol Free Lager Beer 4x330ml',
        description: 'San Miguel Alcohol Free Lager Beer 4x330ml',
        size: '2.65ltr',
        price: 3.5,
        origin: 'Middle East'
    },
    {
        name: 'Erdinger Weissbier Wheat Beer Bottle 500ml',
        description: 'Erdinger Weissbier Wheat Beer Bottle 500ml',
        size: '3.3ltr',
        price: 1.65,
        origin: 'Africa'
    },
    {
        name: 'Kronenbourg 1664 Lager Beer Cans 4 x 568ml Pint',
        description: 'Kronenbourg 1664 Lager Beer Cans 4 x 568ml Pint',
        size: '2.31ltr',
        price: 5.25,
        origin: 'Middle East'
    },
    {
        name: 'Birra Moretti Lager Beer Bottle 660ml',
        description: 'Birra Moretti Lager Beer Bottle 660ml',
        size: '2.88ltr',
        price: 1.9,
        origin: 'Middle East'
    },
    {
        name: "Foster's Lager Beer Cans 4 x 440ml",
        description: "Foster's Lager Beer Cans 4 x 440ml",
        size: '2.13ltr',
        price: 3.75,
        origin: 'North America'
    },
    {
        name: 'Estrella Damm Lager Beer Cans 6x330ml',
        description: 'Estrella Damm Lager Beer Cans 6x330ml',
        size: '3.03ltr',
        price: 6,
        origin: 'Middle East'
    },
    {
        name: 'Budweiser Lager Beer Bottles 6x300ml',
        description: 'Budweiser Lager Beer Bottles 6x300ml',
        size: '2.78ltr',
        price: 5,
        origin: 'Europe'
    },
    {
        name: 'Cobra Premium Beer 620ml',
        description: 'Cobra Premium Beer 620ml',
        size: '3.06ltr',
        price: 1.9,
        origin: 'Australia'
    },
    {
        name: 'San Miguel Premium Lager Beer Bottle 660ml',
        description: 'San Miguel Premium Lager Beer Bottle 660ml',
        size: '2.71ltr',
        price: 1.79,
        origin: 'Europe'
    },
    {
        name: 'Holsten Pils Lager Beer Cans 4x440ml',
        description: 'Holsten Pils Lager Beer Cans 4x440ml',
        size: '2.5ltr',
        price: 4.4,
        origin: 'South America'
    },
    {
        name: 'Coors Lager Beer Bottles x15 330ml',
        description: 'Coors Lager Beer Bottles x15 330ml',
        size: '2.22ltr',
        price: 11,
        origin: 'South America'
    },
    {
        name: 'Carling Original Lager Beer Cans x4 440ml',
        description: 'Carling Original Lager Beer Cans x4 440ml',
        size: '2.05ltr',
        price: 3.6,
        origin: 'Middle East'
    },
    {
        name: 'Corona Lager Beer Cans 6x330ml',
        description: 'Corona Lager Beer Cans 6x330ml',
        size: '3.03ltr',
        price: 6,
        origin: 'Australia'
    },
    {
        name: 'Hobgoblin IPA Beer Cans 4x440ml',
        description: 'Hobgoblin IPA Beer Cans 4x440ml',
        size: '2.84ltr',
        price: 5,
        origin: 'Middle East'
    },
    {
        name: 'Desperados Tequila Lager Beer Bottles 3 x 330ml',
        description: 'Desperados Tequila Lager Beer Bottles 3 x 330ml',
        size: '5.05ltr',
        price: 5,
        origin: 'Africa'
    },
    {
        name: 'Shipyard American IPA Ale Beer Bottle 500ml',
        description: 'Shipyard American IPA Ale Beer Bottle 500ml',
        size: '3.3ltr',
        price: 1.65,
        origin: 'Australia'
    },
    {
        name: 'Carlsberg Special Brew Lager Beer Cans 4x440ml',
        description: 'Carlsberg Special Brew Lager Beer Cans 4x440ml',
        size: '4.43ltr',
        price: 7.8,
        origin: 'Australia'
    },
    {
        name: 'Bud Light Lager Beer cans 4x440ml',
        description: 'Bud Light Lager Beer cans 4x440ml',
        size: '2.27ltr',
        price: 4,
        origin: 'North America'
    },
    {
        name: 'Heineken Premium Lager Beer Cans 6 x 330ml',
        description: 'Heineken Premium Lager Beer Cans 6 x 330ml',
        size: '3.03ltr',
        price: 6,
        origin: 'South America'
    },
    {
        name: 'Warsteiner Premium Lager Beer 660ml Bottle',
        description: 'Warsteiner Premium Lager Beer 660ml Bottle',
        size: '2.88ltr',
        price: 1.9,
        origin: 'South America'
    },
    {
        name: 'Corona Extra Premium Lager Beer bottles 4x330ml',
        description: 'Corona Extra Premium Lager Beer bottles 4x330ml',
        size: '3.79ltr',
        price: 5,
        origin: 'Middle East'
    },
    {
        name: 'Corona Extra Premium Lager Beer Bottles 12x330ml',
        description: 'Corona Extra Premium Lager Beer Bottles 12x330ml',
        size: '3.28ltr',
        price: 13,
        origin: 'Middle East'
    },
    {
        name: 'Stella Artois Premium Lager Beer Cans 18x440ml',
        description: 'Stella Artois Premium Lager Beer Cans 18x440ml',
        size: '2.02ltr',
        price: 16,
        origin: 'Australia'
    },
    {
        name: "Foster's Lager Beer Cans 18 x 440ml",
        description: "Foster's Lager Beer Cans 18 x 440ml",
        size: '1.77ltr',
        price: 14,
        origin: 'Australia'
    },
    {
        name: 'Carling Original Lager Beer Cans x18 440ml',
        description: 'Carling Original Lager Beer Cans x18 440ml',
        size: '1.39ltr',
        price: 11,
        origin: 'Africa'
    },
    {
        name: 'Budweiser Lager Beer Bottles 20x300ml',
        description: 'Budweiser Lager Beer Bottles 20x300ml',
        size: '2.33ltr',
        price: 14,
        origin: 'Australia'
    },
    {
        name: 'San Miguel Premium Lager Beer 10x440ml',
        description: 'San Miguel Premium Lager Beer 10x440ml',
        size: '2.5ltr',
        price: 11,
        origin: 'South America'
    },
    {
        name: 'Birra Moretti Lager Beer Bottle 18x330ml',
        description: 'Birra Moretti Lager Beer Bottle 18x330ml',
        size: '3.37ltr',
        price: 20,
        origin: 'Europe'
    },
    {
        name: 'Kronenbourg 1664 Lager Beer Cans 15 x 440ml',
        description: 'Kronenbourg 1664 Lager Beer Cans 15 x 440ml',
        size: '2.12ltr',
        price: 14,
        origin: 'North America'
    },
    {
        name: 'Estrella Damm Lager Beer Bottles 12x330ml',
        description: 'Estrella Damm Lager Beer Bottles 12x330ml',
        size: '3.54ltr',
        price: 14,
        origin: 'Africa'
    },
    {
        name: 'Stella Artois Lager Beer Bottles 12x330ml',
        description: 'Stella Artois Lager Beer Bottles 12x330ml',
        size: '2.53ltr',
        price: 10,
        origin: 'North America'
    },
    {
        name: 'San Miguel Premium Lager Beer Bottles 12x330ml',
        description: 'San Miguel Premium Lager Beer Bottles 12x330ml',
        size: '2.27ltr',
        price: 9,
        origin: 'South America'
    },
    {
        name: 'Peroni Nastro Azzurro 0.0% Alcohol Free Beer lager Bottles 4x330ml',
        description: 'Peroni Nastro Azzurro 0.0% Alcohol Free Beer lager Bottles 4x330ml',
        size: '3.41ltr',
        price: 4.5,
        origin: 'Europe'
    },

    {
        name: 'Brewdog Mixed Beer Can Pack 12x330ml',
        description: 'Brewdog Mixed Beer Can Pack 12x330ml',
        size: '3.79ltr',
        price: 15,
        origin: 'Europe'
    },
    {
        name: 'Birra Moretti Lager Beer Bottles 12x330ml',
        description: 'Birra Moretti Lager Beer Bottles 12x330ml',
        size: '3.03ltr',
        price: 12,
        origin: 'Australia'
    },
    {
        name: 'Erdinger Alkoholfrei Alcohol Free Wheat Beer 500ml',
        description: 'Erdinger Alkoholfrei Alcohol Free Wheat Beer 500ml',
        size: '3ltr',
        price: 1.5,
        origin: 'North America'
    },
    {
        name: 'Hobgoblin Gold Ale Beer Cans 4x500ml',
        description: 'Hobgoblin Gold Ale Beer Cans 4x500ml',
        size: '2.13ltr',
        price: 4.25,
        origin: 'South America'
    },
    {
        name: 'Corona Extra Premium Lager Beer bottles 18x330ml',
        description: 'Corona Extra Premium Lager Beer bottles 18x330ml',
        size: '2.35ltr',
        price: 14,
        origin: 'South America'
    },
    {
        name: 'Birra Moretti Lager Beer Cans 6 x 330ml',
        description: 'Birra Moretti Lager Beer Cans 6 x 330ml',
        size: '3.28ltr',
        price: 6.5,
        origin: 'Europe'
    },
    {
        name: 'Hobgoblin Ruby Ale Beer Cans 4x500ml',
        description: 'Hobgoblin Ruby Ale Beer Cans 4x500ml',
        size: '2.13ltr',
        price: 4.25,
        origin: 'Middle East'
    },
    {
        name: 'Heineken Premium Lager Beer Bottles 12x330ml',
        description: 'Heineken Premium Lager Beer Bottles 12x330ml',
        size: '2.27ltr',
        price: 9,
        origin: 'Europe'
    },
    {
        name: 'Budweiser Lager Beer Cans 18x440ml',
        description: 'Budweiser Lager Beer Cans 18x440ml',
        size: '1.77ltr',
        price: 14,
        origin: 'Middle East'
    },
    {
        name: 'Leffe Blond Abbey Beer Bottles 4x330ml',
        description: 'Leffe Blond Abbey Beer Bottles 4x330ml',
        size: '3.98ltr',
        price: 5.25,
        origin: 'North America'
    },
    {
        name: 'Hobgoblin Gold Ale BeerBottle 500ml',
        description: 'Hobgoblin Gold Ale Beer Bottle 500ml',
        size: '2.7ltr',
        price: 1.35,
        origin: 'Europe'
    },
    {
        name: 'Kronenbourg 1664 Lager Beer Bottles 15 x 275ml',
        description: 'Kronenbourg 1664 Lager Beer Bottles 15 x 275ml',
        size: '2.18ltr',
        price: 9,
        origin: 'Australia'
    },
    {
        name: 'Birrificio Angelo Poretti Lager Beer 12x330ml',
        description: 'Birrificio Angelo Poretti Lager Beer 12x330ml',
        size: '3.54ltr',
        price: 14,
        origin: 'Europe'
    },
    {
        name: 'San Miguel Premium Lager Beer Bottles 4x330ml',
        description: 'San Miguel Premium Lager Beer Bottles 4x330ml',
        size: '3.03ltr',
        price: 4,
        origin: 'Europe'
    },
    {
        name: 'Brewdog Elvis Juice Grapefruit Beer 4x330ml',
        description: 'Brewdog Elvis Juice Grapefruit Beer 4x330ml',
        size: '4.17ltr',
        price: 5.5,
        origin: 'South America'
    },
    {
        name: '19 Crimes Red Wine 75cl',
        description: '19 Crimes Red Wine 75cl',
        size: '600cl',
        price: 8,
        origin: 'Australia'
    },
    {
        name: 'Brancott Estate Sauvignon Blanc White Wine 75cl',
        description: 'Brancott Estate Sauvignon Blanc White Wine 75cl',
        size: '600cl',
        price: 8,
        origin: 'North America'
    },
    {
        name: 'Campo Viejo Rioja Garnacha Red Wine 75cl ',
        description: 'Campo Viejo Rioja Garnacha Red Wine 75cl ',
        size: '600cl',
        price: 8,
        origin: 'North America'
    },
    {
        name: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
        description: 'Campo Viejo Rioja Tempranillo Red Wine 75cl',
        size: '525cl',
        price: 7,
        origin: 'Australia'
    },
    {
        name: "Sainsbury's Languedoc White Wine, Taste the Difference 75cl",
        description: "Sainsbury's Languedoc White Wine, Taste the Difference 75cl",
        size: '506.25cl',
        price: 6.75,
        origin: 'North America'
    },
    {
        name: 'Rutas de Cafayate Malbec Red Wine 750ml',
        description: 'Rutas de Cafayate Malbec Red Wine 750ml',
        size: '600cl',
        price: 8,
        origin: 'Africa'
    },
    {
        name: 'Campo Viejo Rioja Blanco White Wine 75cl',
        description: 'Campo Viejo Rioja Blanco White Wine 75cl',
        size: '525cl',
        price: 7,
        origin: 'Australia'
    },
    {
        name: "Jacob's Creek Double Barrel Matured Shiraz Red Wine 75cl",
        description: "Jacob's Creek Double Barrel Matured Shiraz Red Wine 75cl",
        size: '900cl',
        price: 12,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Ginger Wine 70cl",
        description: "Sainsbury's Ginger Wine 70cl",
        size: '281.25cl',
        price: 3.5,
        origin: 'South America'
    },
    {
        name: "Hellmann's Plant Based Vegan Mayonnaise 270g",
        description: "Hellmann's Plant Based Vegan Mayonnaise 270g",
        size: '93ml',
        price: 2.6,
        origin: 'Middle East'
    },
    {
        name: 'Pure Dairy Free Vegan Olive Spread 500g',
        description: 'Pure Dairy Free Vegan Olive Spread 500g',
        size: '3.6kg',
        price: 1.8,
        origin: 'Europe'
    },
    {
        name: 'Quorn Vegan Smoky Ham Free Slices 100g',
        description: 'Quorn Vegan Smoky Ham Free Slices 100g',
        size: '24kg',
        price: 2.4,
        origin: 'North America'
    },
    {
        name: 'Richmond Thick Frozen Meat Free Vegan Sausages x8 320g',
        description: 'Richmond Thick Frozen Meat Free Vegan Sausages x8 320g',
        size: '5.47kg',
        price: 1.75,
        origin: 'Africa'
    },
    {
        name: 'Swedish Glace Dairy Free Vegan Ice Cream Tub, Smooth Vanilla 750ml',
        description: 'Swedish Glace Dairy Free Vegan Ice Cream Tub, Smooth Vanilla 750ml',
        size: '33ml',
        price: 2.5,
        origin: 'Europe'
    },
    {
        name: 'Cauldron Vegan Marinated Tofu Pieces 160g',
        description: 'Cauldron Vegan Marinated Tofu Pieces 160g',
        size: '12.5kg',
        price: 2,
        origin: 'Europe'
    },
    {
        name: 'The Vegetarian Butcher Happy Go Clucky Vegan Chicken Burger 180g',
        description: 'The Vegetarian Butcher Happy Go Clucky Vegan Chicken Burger 180g',
        size: '13.89kg',
        price: 2.5,
        origin: 'Middle East'
    },
    {
        name: 'Stork Vegan Baking Block 250g',
        description: 'Stork Vegan Baking Block 250g',
        size: '5kg',
        price: 1.25,
        origin: 'Africa'
    },
    {
        name: 'Richmond Meat Free Vegan No Ham Deli Slices x6 90g',
        description: 'Richmond Meat Free Vegan No Ham Deli Slices x6 90g',
        size: '22.22kg',
        price: 2,
        origin: 'Middle East'
    },
    {
        name: 'Eat Natural Simply Vegan Peanuts, Coconut and Chocolate Fruit & Nut Bars 3 x 45g',
        description: 'Eat Natural Simply Vegan Peanuts, Coconut and Chocolate Fruit & Nut Bars 3 x 45g',
        size: '148g',
        price: 2,
        origin: 'Australia'
    },
    {
        name: "Sainsbury's Rich Vegan Deli-Style Coleslaw 300g",
        description: "Sainsbury's Rich Vegan Deli-Style Coleslaw 300g",
        size: '40g',
        price: 1.2,
        origin: 'Australia'
    },
    {
        name: 'Richmond Meat Free Vegan Sausages x8 336g',
        description: 'Richmond Meat Free Vegan Sausages x8 336g',
        size: '6.55kg',
        price: 2.2,
        origin: 'Australia'
    },
    {
        name: 'Flora Lighter Vegan Spread 500g',
        description: 'Flora Lighter Vegan Spread 500g',
        size: '3kg',
        price: 1.5,
        origin: 'South America'
    },
    {
        name: 'Elmlea Plant Double Vegan Alternative to Cream 250ml',
        description: 'Elmlea Plant Double Vegan Alternative to Cream 250ml',
        size: '52ml',
        price: 1.3,
        origin: 'Europe'
    },
    {
        name: 'Cauldron Vegan Tofu Block 396g',
        description: 'Cauldron Vegan Tofu Block 396g',
        size: '6.31kg',
        price: 2.5,
        origin: 'Europe'
    },
    {
        name: 'Violife Original Flavour Slices Vegan Alternative to Cheese 200g',
        description: 'Violife Original Flavour Slices Vegan Alternative to Cheese 200g',
        size: '13.75kg',
        price: 2.75,
        origin: 'Europe'
    },
    {
        name: 'Richmond Meat Free Vegan Smoked Bacon Rashers x8 150g',
        description: 'Richmond Meat Free Vegan Smoked Bacon Rashers x8 150g',
        size: '18kg',
        price: 2.7,
        origin: 'Europe'
    },
    {
        name: 'Quorn Vegan Chicken Free Slices 100g',
        description: 'Quorn Vegan Chicken Free Slices 100g',
        size: '24kg',
        price: 2.4,
        origin: 'Australia'
    },
    {
        name: 'The Vegetarian Butcher What The Cluck Vegan Chicken Chunks 160g',
        description: 'The Vegetarian Butcher What The Cluck Vegan Chicken Chunks 160g',
        size: '18.75kg',
        price: 3,
        origin: 'Europe'
    },
    {
        name: 'Naturli Vegan Spread, Organic 450g',
        description: 'Naturli Vegan Spread, Organic 450g',
        size: '8.33kg',
        price: 3.75,
        origin: 'South America'
    },
    {
        name: 'Chicken Kyiv',
        description: 'Chicken meal.',
        price: 3.99,
        size: '250g',
        origin: 'Europe'
    },
    {
        name: 'Woodland Free Range Large Eggs',
        description: 'Free range British eggs.',
        price: 2.2,
        size: 'x12',
        origin: 'Europe'
    },
    {
        name: 'Basmati Rice',
        description: 'Basmati White Rice.',
        price: 1.85,
        size: '1kg',
        origin: 'Europe'
    },
    {
        name: 'Mango',
        description: 'Sweet and delicious mango.',
        price: 0.65,
        size: '80g',
        origin: 'South America'
    },
    {
        name: 'Vitalite Dairy Free Cheese',
        description: 'Dairy Free alternative to Cheese with added calcium.',
        price: 2,
        size: '200g',
        origin: 'Europe'
    },
    {
        name: 'Heinz Baked Beans 3x200g',
        description: 'Heinz Baked Beans 3x200g',
        size: '3.08kg',
        price: 1.85,
        origin: 'Europe'
    },
    {
        name: "Hubbard's Foodstore Baked Beans In Tomato Sauce, 400g",
        description: "Hubbard's Foodstore Baked Beans In Tomato Sauce, 400g",
        size: '0.53kg',
        price: 0.21,
        origin: 'Europe'
    },
    {
        name: 'Heinz Baked Beans 415g',
        description: 'Heinz Baked Beans 415g',
        size: '2.41kg',
        price: 1,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Baked Beans In Tomato Sauce 200g",
        description: "Sainsbury's Baked Beans In Tomato Sauce 200g",
        size: '1.5kg',
        price: 0.3,
        origin: 'Africa'
    },
    {
        name: 'Branston Baked Beans 4x410g',
        description: 'Branston Baked Beans 4x410g',
        size: '1.22kg',
        price: 2,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Baked Beans In Reduced Sugar & Salt, Tomato Sauce 200g",
        description: "Sainsbury's Baked Beans In Reduced Sugar & Salt, Tomato Sauce 200g",
        size: '1.5kg',
        price: 0.3,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Reduced Sugar & Salt Baked Beans In Tomato Sauce 4x400g",
        description: "Sainsbury's Reduced Sugar & Salt Baked Beans In Tomato Sauce 4x400g",
        size: '0.75kg',
        price: 1.2,
        origin: 'Europe'
    },

    {
        name: "Sainsbury's Reduced Sugar & Salt Baked Beans In Tomato Sauce 400g",
        description: "Sainsbury's Reduced Sugar & Salt Baked Beans In Tomato Sauce 400g",
        size: '0.88kg',
        price: 0.35,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Baked Beans In Tomato Sauce 4x400g",
        description: "Sainsbury's Baked Beans In Tomato Sauce 4x400g",
        size: '0.75kg',
        price: 1.2,
        origin: 'Africa'
    },
    {
        name: 'Branston Baked Beans 220g',
        description: 'Branston Baked Beans 220g',
        size: '2.27kg',
        price: 0.5,
        origin: 'South America'
    },
    {
        name: "Sainsbury's Baked Beans In Tomato Sauce With Sausages 400g",
        description: "Sainsbury's Baked Beans In Tomato Sauce With Sausages 400g",
        size: '2kg',
        price: 0.8,
        origin: 'Africa'
    },
    {
        name: 'Heinz Baked Beans In Tomato Sauce 200g',
        description: 'Heinz Baked Beans In Tomato Sauce 200g',
        size: '3.75kg',
        price: 0.75,
        origin: 'Middle East'
    },
    {
        name: 'Heinz Baked Beans In Tomato Sauce 150g',
        description: 'Heinz Baked Beans In Tomato Sauce 150g',
        size: '3.67kg',
        price: 0.55,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Baked Beans in Tomato Sauce 400g",
        description: "Sainsbury's Baked Beans in Tomato Sauce 400g",
        size: '0.88kg',
        price: 0.35,
        origin: 'Australia'
    },
    {
        name: 'Heinz Baked Beans With Pork Sausages 415g',
        description: 'Heinz Baked Beans With Pork Sausages 415g',
        size: '3.37kg',
        price: 1.4,
        origin: 'Africa'
    },
    {
        name: 'Heinz Baked Beans With Pork Sausages 200g',
        description: 'Heinz Baked Beans With Pork Sausages 200g',
        size: '4.5kg',
        price: 0.9,
        origin: 'South America'
    },
    {
        name: 'Branston Baked Beans 410g',
        description: 'Branston Baked Beans 410g',
        size: '1.46kg',
        price: 0.6,
        origin: 'Australia'
    },
    {
        name: "Sainsbury's Baked Beans, SO Organic 400g",
        description: "Sainsbury's Baked Beans, SO Organic 400g",
        size: '1.88kg',
        price: 0.75,
        origin: 'Europe'
    },
    {
        name: 'Heinz No Added Sugar Baked Beans in a Rich Tomato Sauce 6x415g',
        description: 'Heinz No Added Sugar Baked Beans in a Rich Tomato Sauce 6x415g',
        size: '1.61kg',
        price: 4,
        origin: 'Australia'
    },
    {
        name: 'Branston Baked Beans in a Rich & Tasty Tomato Sauce 6x410g',
        description: 'Branston Baked Beans in a Rich & Tasty Tomato Sauce 6x410g',
        size: '1.2kg',
        price: 2.95,
        origin: 'North America'
    },
    {
        name: 'Heinz Baked Beans, Organic 415g',
        description: 'Heinz Baked Beans, Organic 415g',
        size: '2.77kg',
        price: 1.15,
        origin: 'Africa'
    },
    {
        name: 'Branston Baked Beans & Sausages 405g',
        description: 'Branston Baked Beans & Sausages 405g',
        size: '27g',
        price: 1.1,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Baked Beans With Meatfree Sausages 400g",
        description: "Sainsbury's Baked Beans With Meatfree Sausages 400g",
        size: '2kg',
        price: 0.8,
        origin: 'North America'
    },
    {
        name: 'Branston Reduced Sugar & Salt Baked Beans 410g',
        description: 'Branston Reduced Sugar & Salt Baked Beans 410g',
        size: '1.46kg',
        price: 0.6,
        origin: 'North America'
    },
    {
        name: 'Branston Baked Beans with Sausages 220g',
        description: 'Branston Baked Beans with Sausages 220g',
        size: '36g',
        price: 0.8,
        origin: 'Australia'
    },
    {
        name: "Rakusen's Baked Beans in Tomato Sauce 400g",
        description: "Rakusen's Baked Beans in Tomato Sauce 400g",
        size: '25g',
        price: 1,
        origin: 'Australia'
    },
    {
        name: 'Heinz No Added Sugar Baked Beanz x3 200g',
        description: 'Heinz No Added Sugar Baked Beanz x3 200g',
        size: '3.08kg',
        price: 1.85,
        origin: 'Africa'
    },
    {
        name: 'Heinz No Added Sugar Beans in a Rich Tomato Sauce 4x415g',
        description: 'Heinz No Added Sugar Beans in a Rich Tomato Sauce 4x415g',
        size: '1.81kg',
        price: 3,
        origin: 'Middle East'
    },
    {
        name: 'Heinz No Added Sugar Beans in a Rich Tomato Sauce 415g',
        description: 'Heinz No Added Sugar Beans in a Rich Tomato Sauce 415g',
        size: '2.41kg',
        price: 1,
        origin: 'Australia'
    },
    {
        name: 'Walkers Smoky Bacon Multipack Crisps 6x25g',
        description: 'Walkers Smoky Bacon Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'South America'
    },
    {
        name: 'Walkers Less Salt Mild Cheese & Onion Multipack Crisps 6x25g',
        description: 'Walkers Less Salt Mild Cheese & Onion Multipack Crisps 6x25g',
        size: '130g',
        price: 1.95,
        origin: 'Australia'
    },
    {
        name: 'Popchips Barbeque Multipack Crisps 5pk',
        description: 'Popchips Barbeque Multipack Crisps 5pk',
        size: '177g',
        price: 1.5,
        origin: 'Middle East'
    },
    {
        name: "Jacob's Crinklys Cheese & Onion Crisps 6x25g",
        description: "Jacob's Crinklys Cheese & Onion Crisps 6x25g",
        size: '83g',
        price: 1.25,
        origin: 'Middle East'
    },
    {
        name: 'Tyrrells Lightly Sea Salted Sharing Crisps 150g',
        description: 'Tyrrells Lightly Sea Salted Sharing Crisps 150g',
        size: '153g',
        price: 2.3,
        origin: 'Middle East'
    },
    {
        name: 'Walkers Roast Chicken Multipack Crisps 6x25g',
        description: 'Walkers Roast Chicken Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Assorted Crisps 6x25g",
        description: "Sainsbury's Assorted Crisps 6x25g",
        size: '63g',
        price: 0.95,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Salt & Vinegar Crisps 6x25g",
        description: "Sainsbury's Salt & Vinegar Crisps 6x25g",
        size: '63g',
        price: 0.95,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Ready Salted Crisps 12x25g",
        description: "Sainsbury's Ready Salted Crisps 12x25g",
        size: '58g',
        price: 1.75,
        origin: 'Africa'
    },
    {
        name: 'Sensations Roast Chicken & Thyme Sharing Crisps 150g',
        description: 'Sensations Roast Chicken & Thyme Sharing Crisps 150g',
        size: '83g',
        price: 1.25,
        origin: 'Europe'
    },
    {
        name: 'Walkers Meaty Variety Multipack Crisps 22x25g',
        description: 'Walkers Meaty Variety Multipack Crisps 22x25g',
        size: '73g',
        price: 4,
        origin: 'Europe'
    },
    {
        name: 'Popchips Sea Salt & Vinegar Sharing Crisps 85g',
        description: 'Popchips Sea Salt & Vinegar Sharing Crisps 85g',
        size: '235g',
        price: 2,
        origin: 'North America'
    },
    {
        name: 'Walkers Sensations Thai Sweet Chilli Multipack Crisps 5x25g',
        description: 'Walkers Sensations Thai Sweet Chilli Multipack Crisps 5x25g',
        size: '108g',
        price: 1.35,
        origin: 'Australia'
    },
    {
        name: 'Doritos Tangy Cheese Sharing Tortilla Chips Crisps 180g',
        description: 'Doritos Tangy Cheese Sharing Tortilla Chips Crisps 180g',
        size: '111g',
        price: 2,
        origin: 'Europe'
    },
    {
        name: 'Walkers Marmite Multipack Crisps 6x25g',
        description: 'Walkers Marmite Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'Middle East'
    },
    {
        name: "Jacob's Twiglets Crisps 6x24g",
        description: "Jacob's Twiglets Crisps 6x24g",
        size: '104g',
        price: 1.5,
        origin: 'Africa'
    },
    {
        name: 'Pringles Paprika Sharing Crisps 200g',
        description: 'Pringles Paprika Sharing Crisps 200g',
        size: '100g',
        price: 2,
        origin: 'North America'
    },
    {
        name: 'Hula Hoops Original Crisps 12x24g',
        description: 'Hula Hoops Original Crisps 12x24g',
        size: '87g',
        price: 2.5,
        origin: 'South America'
    },
    {
        name: "Sainsbury's Classic Variety Assorted Crisps 12x25g",
        description: "Sainsbury's Classic Variety Assorted Crisps 12x25g",
        size: '58g',
        price: 1.75,
        origin: 'Europe'
    },
    {
        name: 'Kettle Chips Sour Cream & Sweet Onion Sharing Crisps 150g',
        description: 'Kettle Chips Sour Cream & Sweet Onion Sharing Crisps 150g',
        size: '133g',
        price: 2,
        origin: 'North America'
    },
    {
        name: "Jacob's Cracker Crisps Salt & Vinegar 150g",
        description: "Jacob's Cracker Crisps Salt & Vinegar 150g",
        size: '83g',
        price: 1.25,
        origin: 'Middle East'
    },
    {
        name: 'Walkers Ready Salted Multipack Crisps 6x25g',
        description: 'Walkers Ready Salted Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'Africa'
    },
    {
        name: 'Pom-Bear Original Multipack Crisps 6x13g',
        description: 'Pom-Bear Original Multipack Crisps 6x13g',
        size: '224g',
        price: 1.75,
        origin: 'Africa'
    },
    {
        name: 'Walkers Cheese & Onion Multipack Crisps 6x25g',
        description: 'Walkers Cheese & Onion Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'Africa'
    },
    {
        name: 'Kettle Chips Lightly Salted Sharing Crisps 150g',
        description: 'Kettle Chips Lightly Salted Sharing Crisps 150g',
        size: '133g',
        price: 2,
        origin: 'Australia'
    },
    {
        name: 'Pringles Original Sharing Crisps 200g',
        description: 'Pringles Original Sharing Crisps 200g',
        size: '100g',
        price: 2,
        origin: 'North America'
    },
    {
        name: 'Walkers Classic Variety Multipack Crisps 12x25g',
        description: 'Walkers Classic Variety Multipack Crisps 12x25g',
        size: '83g',
        price: 2.5,
        origin: 'Middle East'
    },
    {
        name: 'Doritos Cool Original Sharing Tortilla Chips Crisps 180g',
        description: 'Doritos Cool Original Sharing Tortilla Chips Crisps 180g',
        size: '110g',
        price: 2,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Ready Salted Crisps 6x25g",
        description: "Sainsbury's Ready Salted Crisps 6x25g",
        size: '63g',
        price: 0.95,
        origin: 'Australia'
    },
    {
        name: 'Sensations Thai Sweet Chilli Sharing Crisps 150g',
        description: 'Sensations Thai Sweet Chilli Sharing Crisps 150g',
        size: '83g',
        price: 1.25,
        origin: 'South America'
    },
    {
        name: 'Hula Hoops Original Potato Ring Crisps 6x24g',
        description: 'Hula Hoops Original Potato Ring Crisps 6x24g',
        size: '122g',
        price: 1.75,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Sea Salt & Cider Vinegar Crisps, Taste the Difference 150g",
        description: "Sainsbury's Sea Salt & Cider Vinegar Crisps, Taste the Difference 150g",
        size: '67g',
        price: 1,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Cheddar & Spring Onion Crisps, Taste the Difference 150g",
        description: "Sainsbury's Cheddar & Spring Onion Crisps, Taste the Difference 150g",
        size: '67g',
        price: 1,
        origin: 'South America'
    },
    {
        name: 'Kettle Chips Sea Salt & Balsamic Vinegar of Modena Sharing Crisps 150g',
        description: 'Kettle Chips Sea Salt & Balsamic Vinegar of Modena Sharing Crisps 150g',
        size: '133g',
        price: 2,
        origin: 'Africa'
    },
    {
        name: 'Walkers Prawn Cocktail Multipack Crisps 6x25g',
        description: 'Walkers Prawn Cocktail Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'Africa'
    },
    {
        name: 'Walkers Salt & Vinegar Multipack Crisps 6x25g',
        description: 'Walkers Salt & Vinegar Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'Middle East'
    },
    {
        name: 'Walkers Classic Variety Multipack Crisps 22x25g',
        description: 'Walkers Classic Variety Multipack Crisps 22x25g',
        size: '73g',
        price: 4,
        origin: 'Australia'
    },
    {
        name: 'Hula Hoops BBQ Beef Potato Ring Crisps 6x24g',
        description: 'Hula Hoops BBQ Beef Potato Ring Crisps 6x24g',
        size: '122g',
        price: 1.75,
        origin: 'Middle East'
    },
    {
        name: 'Doritos Chilli Heatwave Sharing Tortilla Chips Crisps 180g',
        description: 'Doritos Chilli Heatwave Sharing Tortilla Chips Crisps 180g',
        size: '180g',
        price: 2,
        origin: 'Middle East'
    },
    {
        name: 'Hula Hoops Variety Crisps 12x24g',
        description: 'Hula Hoops Variety Crisps 12x24g',
        size: '87g',
        price: 2.5,
        origin: 'Australia'
    },
    {
        name: 'Walkers Salt & Shake Multipack Crisps 6x24g',
        description: 'Walkers Salt & Shake Multipack Crisps 6x24g',
        size: '93g',
        price: 1.35,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Cheese & Onion Crisps 6x25g",
        description: "Sainsbury's Cheese & Onion Crisps 6x25g",
        size: '63g',
        price: 0.95,
        origin: 'South America'
    },
    {
        name: 'Pringles Salt & Vinegar Sharing Crisps 200g',
        description: 'Pringles Salt & Vinegar Sharing Crisps 200g',
        size: '100g',
        price: 2,
        origin: 'Africa'
    },
    {
        name: 'Kettle Chips Sea Salt & Crushed Black Peppercorns Sharing Crisps 150g',
        description: 'Kettle Chips Sea Salt & Crushed Black Peppercorns Sharing Crisps 150g',
        size: '133g',
        price: 2,
        origin: 'Africa'
    },
    {
        name: "McCoy's Flame Grilled Steak Variety Pack Crisps 6x25g",
        description: "McCoy's Flame Grilled Steak Variety Pack Crisps 6x25g",
        size: '100g',
        price: 1.5,
        origin: 'Australia'
    },
    {
        name: 'Walkers Ready Salted Crisps 12x25g',
        description: 'Walkers Ready Salted Crisps 12x25g',
        size: '83g',
        price: 2.5,
        origin: 'North America'
    },
    {
        name: 'Walkers Less Salt Lightly Salted Multipack Crisps 6x25g',
        description: 'Walkers Less Salt Lightly Salted Multipack Crisps 6x25g',
        size: '130g',
        price: 1.95,
        origin: 'Middle East'
    },
    {
        name: "McCoy's Classic Variety Pack Crisps 6x25g",
        description: "McCoy's Classic Variety Pack Crisps 6x25g",
        size: '100g',
        price: 1.5,
        origin: 'Africa'
    },
    {
        name: 'Kettle Chips Mature Cheddar & Red Onion Sharing Crisps 150g',
        description: 'Kettle Chips Mature Cheddar & Red Onion Sharing Crisps 150g',
        size: '133g',
        price: 2,
        origin: 'North America'
    },
    {
        name: 'Kettle Chips Sweet Chilli & Sour Cream Sharing Crisps 150g',
        description: 'Kettle Chips Sweet Chilli & Sour Cream Sharing Crisps 150g',
        size: '133g',
        price: 2,
        origin: 'Europe'
    },
    {
        name: "Sainsbury's Classic Variety Assorted Crisps 22x25g",
        description: "Sainsbury's Classic Variety Assorted Crisps 22x25g",
        size: '54g',
        price: 2.95,
        origin: 'Middle East'
    },
    {
        name: 'Walkers Classic Variety Multipack Crisps 6x25g',
        description: 'Walkers Classic Variety Multipack Crisps 6x25g',
        size: '90g',
        price: 1.35,
        origin: 'North America'
    },
    {
        name: 'Kettle Chips Lightly Salted Multipack Crisps 5x30g',
        description: 'Kettle Chips Lightly Salted Multipack Crisps 5x30g',
        size: '120g',
        price: 1.8,
        origin: 'Africa'
    },
    {
        name: 'Popchips Barbeque Sharing Crisps 85g',
        description: 'Popchips Barbeque Sharing Crisps 85g',
        size: '235g',
        price: 2,
        origin: 'Middle East'
    },
    {
        name: "McCoy's Salt & Vinegar Variety Pack Crisps 6x25g",
        description: "McCoy's Salt & Vinegar Variety Pack Crisps 6x25g",
        size: '100g',
        price: 1.5,
        origin: 'North America'
    },
    {
        name: "Kellogg's Crunchy Nut Honey & Nut Clusters Breakfast Cereal 450g",
        description: "Kellogg's Crunchy Nut Honey & Nut Clusters Breakfast Cereal 450g",
        size: '450g',
        price: 2.5,
        origin: 'Middle East'
    },
    {
        name: "Kellogg's Krave Milk Chocolate Cereal 410g",
        description: "Kellogg's Krave Milk Chocolate Cereal 410g",
        size: '67g',
        price: 2.5,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Wheats Apricot Breakfast Cereal 500g",
        description: "Kellogg's Wheats Apricot Breakfast Cereal 500g",
        size: '36g',
        price: 1.8,
        origin: 'Africa'
    },
    {
        name: 'Kellogg’s Frosted Wheats Cereal 500g',
        description: 'Kellogg’s Frosted Wheats Cereal 500g',
        size: '36g',
        price: 1.8,
        origin: 'South America'
    },
    {
        name: 'Nestle Nesquik Chocolate Cereal 375g',
        description: 'Nestle Nesquik Chocolate Cereal 375g',
        size: '71g',
        price: 2.65,
        origin: 'Africa'
    },
    {
        name: 'Weetabix Chocolate Cereal x24 540g',
        description: 'Weetabix Chocolate Cereal x24 540g',
        size: '0.13bisc',
        price: 3.2,
        origin: 'Middle East'
    },
    {
        name: 'Kellogg’s Raisin Wheats Cereal 450g',
        description: 'Kellogg’s Raisin Wheats Cereal 450g',
        size: '40g',
        price: 1.8,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Corn Flakes Breakfast Cereal 250g",
        description: "Kellogg's Corn Flakes Breakfast Cereal 250g",
        size: '60g',
        price: 1.5,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Puffed Wheat Cereal 160g",
        description: "Sainsbury's Puffed Wheat Cereal 160g",
        size: '50g',
        price: 0.8,
        origin: 'South America'
    },
    {
        name: "Sainsbury's Choco Hazelnut Squares Cereal 375g",
        description: "Sainsbury's Choco Hazelnut Squares Cereal 375g",
        size: '37g',
        price: 1.4,
        origin: 'North America'
    },
    {
        name: "Kellogg's Coco Pops Cereal 375g",
        description: "Kellogg's Coco Pops Cereal 375g",
        size: '60g',
        price: 2.25,
        origin: 'Africa'
    },
    {
        name: 'Nestle Shredded Wheat Honey Nut Cereal 500g',
        description: 'Nestle Shredded Wheat Honey Nut Cereal 500g',
        size: '500g',
        price: 2.8,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Choco Hoops Cereal 375g",
        description: "Sainsbury's Choco Hoops Cereal 375g",
        size: '31g',
        price: 1.15,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Bran Flakes Cereal 500g",
        description: "Kellogg's Bran Flakes Cereal 500g",
        size: '40g',
        price: 2,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Frosted Flakes Cereal 500g",
        description: "Sainsbury's Frosted Flakes Cereal 500g",
        size: '20g',
        price: 1,
        origin: 'Africa'
    },
    {
        name: 'Weetabix Crunchy Bran Cereal 375g',
        description: 'Weetabix Crunchy Bran Cereal 375g',
        size: '40g',
        price: 1.5,
        origin: 'Africa'
    },
    {
        name: "Kellogg's Krave Chocolate Hazelnut Cereal 410g",
        description: "Kellogg's Krave Chocolate Hazelnut Cereal 410g",
        size: '61g',
        price: 2.5,
        origin: 'Africa'
    },
    {
        name: 'Weetabix Protein Cereal x24',
        description: 'Weetabix Protein Cereal x24',
        size: '0.1bisc',
        price: 2.5,
        origin: 'Middle East'
    },
    {
        name: "Kellogg's Special K Original Breakfast Cereal 375g",
        description: "Kellogg's Special K Original Breakfast Cereal 375g",
        size: '60g',
        price: 2.25,
        origin: 'Middle East'
    },
    {
        name: "Sainsbury's Honey Hoops Cereal 375g",
        description: "Sainsbury's Honey Hoops Cereal 375g",
        size: '32g',
        price: 1.2,
        origin: 'Africa'
    },
    {
        name: "Kellogg's All-Bran Golden Crunch Cereal 390g",
        description: "Kellogg's All-Bran Golden Crunch Cereal 390g",
        size: '51g',
        price: 2,
        origin: 'Middle East'
    },
    {
        name: "Kellogg's Frosties Breakfast Cereal 1kg",
        description: "Kellogg's Frosties Breakfast Cereal 1kg",
        size: '40g',
        price: 4,
        origin: 'Middle East'
    },
    {
        name: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
        description: 'Cookie Crisp Chocolatey Chip Cookie Cereal 500g',
        size: '42g',
        price: 2.1,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Mini Cinnamon Buns Cereal, Limited Edition 375g",
        description: "Sainsbury's Mini Cinnamon Buns Cereal, Limited Edition 375g",
        size: '40g',
        price: 1.5,
        origin: 'North America'
    },
    {
        name: "Kellogg's Coco Pops Rocks Breakfast Cereal 350g",
        description: "Kellogg's Coco Pops Rocks Breakfast Cereal 350g",
        size: '56g',
        price: 2,
        origin: 'Europe'
    },
    {
        name: 'Weetabix Cereal x72',
        description: 'Weetabix Cereal x72',
        size: '0.1bisc',
        price: 7,
        origin: 'Europe'
    },
    {
        name: 'Nestle Cheerios Honey Cereal 375g',
        description: 'Nestle Cheerios Honey Cereal 375g',
        size: '72g',
        price: 2.65,
        origin: 'Africa'
    },
    {
        name: 'Weetabix Cereal x24',
        description: 'Weetabix Cereal x24',
        size: '0.13bisc',
        price: 3,
        origin: 'Middle East'
    },
    {
        name: "Kellogg's Corn Flakes Breakfast Cereal 500g",
        description: "Kellogg's Corn Flakes Breakfast Cereal 500g",
        size: '40g',
        price: 2,
        origin: 'Australia'
    },
    {
        name: 'Weetabix Cereal x48',
        description: 'Weetabix Cereal x48',
        size: '0.08bisc',
        price: 4,
        origin: 'Europe'
    },
    {
        name: "Sainsbury's Wholegrain Malties Cereal 750g",
        description: "Sainsbury's Wholegrain Malties Cereal 750g",
        size: '16g',
        price: 1.2,
        origin: 'Australia'
    },
    {
        name: "Kellogg's Special K Original Breakfast Cereal 750g",
        description: "Kellogg's Special K Original Breakfast Cereal 750g",
        size: '47g',
        price: 3.5,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Wholewheat Biscuits Cereal x48 860g",
        description: "Sainsbury's Wholewheat Biscuits Cereal x48 860g",
        size: '0.06bisc',
        price: 2.75,
        origin: 'North America'
    },
    {
        name: "Sainsbury's Cornflakes Cereal 500g",
        description: "Sainsbury's Cornflakes Cereal 500g",
        size: '20g',
        price: 1,
        origin: 'Africa'
    },
    {
        name: "Sainsbury's Multigrain Hoops Cereal 375g",
        description: "Sainsbury's Multigrain Hoops Cereal 375g",
        size: '32g',
        price: 1.2,
        origin: 'South America'
    },
    {
        name: "Kellogg's Frosties Breakfast Cereal 500g",
        description: "Kellogg's Frosties Breakfast Cereal 500g",
        size: '50g',
        price: 2.5,
        origin: 'Australia'
    },
    {
        name: "Sainsbury's Wholewheat Biscuits, Cereal x24 430g",
        description: "Sainsbury's Wholewheat Biscuits, Cereal x24 430g",
        size: '0.08bisc',
        price: 1.95,
        origin: 'South America'
    },
    {
        name: 'Nestle Shredded Wheat Bitesize Cereal 720g',
        description: 'Nestle Shredded Wheat Bitesize Cereal 720g',
        size: '38g',
        price: 2.75,
        origin: 'Africa'
    },
    {
        name: "Kellogg's Rice Krispies Breakfast Cereal 700g",
        description: "Kellogg's Rice Krispies Breakfast Cereal 700g",
        size: '50g',
        price: 3.5,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Special K Red Berries Breakfast Cereal 500g",
        description: "Kellogg's Special K Red Berries Breakfast Cereal 500g",
        size: '500g',
        price: 3.5,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Crunchy Nut Breakfast Cereal 720g",
        description: "Kellogg's Crunchy Nut Breakfast Cereal 720g",
        size: '720g',
        price: 4,
        origin: 'Middle East'
    },
    {
        name: 'Shredded Wheat Cereal Biscuits x30',
        description: 'Shredded Wheat Cereal Biscuits x30',
        size: '0.1bisc',
        price: 2.95,
        origin: 'Europe'
    },
    {
        name: 'Weetabix Weetos Chocolatey Cereal 500g',
        description: 'Weetabix Weetos Chocolatey Cereal 500g',
        size: '58g',
        price: 2.9,
        origin: 'Middle East'
    },
    {
        name: "Kellogg's Bran Flakes Breakfast Cereal 750g",
        description: "Kellogg's Bran Flakes Breakfast Cereal 750g",
        size: '40g',
        price: 3,
        origin: 'North America'
    },
    {
        name: 'Weetabix Cereal x12',
        description: 'Weetabix Cereal x12',
        size: '0.13bisc',
        price: 1.6,
        origin: 'Middle East'
    },
    {
        name: "Kellogg's Crunchy Nut Breakfast Cereal 375g",
        description: "Kellogg's Crunchy Nut Breakfast Cereal 375g",
        size: '60g',
        price: 2.25,
        origin: 'Europe'
    },
    {
        name: "Kellogg's All-Bran Breakfast Cereal 750g",
        description: "Kellogg's All-Bran Breakfast Cereal 750g",
        size: '40g',
        price: 3,
        origin: 'North America'
    },
    {
        name: 'Nestle Curiously Cinnamon Cereal 375g',
        description: 'Nestle Curiously Cinnamon Cereal 375g',
        size: '73g',
        price: 2.75,
        origin: 'North America'
    },
    {
        name: "Kellogg's Corn Flakes Breakfast Cereal 1kg",
        description: "Kellogg's Corn Flakes Breakfast Cereal 1kg",
        size: '35g',
        price: 3.5,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Wheats Blueberry Breakfast Cereal 500g",
        description: "Kellogg's Wheats Blueberry Breakfast Cereal 500g",
        size: '500g',
        price: 1.8,
        origin: 'Europe'
    },
    {
        name: 'Weetabix Oatibix Flakes Golden Oat Flakes Cereal 550g',
        description: 'Weetabix Oatibix Flakes Golden Oat Flakes Cereal 550g',
        size: '550g',
        price: 3,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Coco Pops Breakfast Cereal 720g",
        description: "Kellogg's Coco Pops Breakfast Cereal 720g",
        size: '49g',
        price: 3.5,
        origin: 'Africa'
    },
    {
        name: "Kellogg's Cereal Variety Pack x8 185g",
        description: "Kellogg's Cereal Variety Pack x8 185g",
        size: '78g',
        price: 1.5,
        origin: 'Australia'
    },
    {
        name: "Kellogg's Rice Krispies Multigrain Shapes Breakfast Cereal 350g",
        description: "Kellogg's Rice Krispies Multigrain Shapes Breakfast Cereal 350g",
        size: '56g',
        price: 2,
        origin: 'South America'
    },
    {
        name: 'Nestle Honey Cheerios Cereal 515g',
        description: 'Nestle Honey Cheerios Cereal 515g',
        size: '62g',
        price: 3.2,
        origin: 'South America'
    },
    {
        name: 'Jordans Country Crisp With Flame Raisins Cereal 500g',
        description: 'Jordans Country Crisp With Flame Raisins Cereal 500g',
        size: '40g',
        price: 2,
        origin: 'Europe'
    },
    {
        name: "Kellogg's Plain Wheats Cereal 500g",
        description: "Kellogg's Plain Wheats Cereal 500g",
        size: '36g',
        price: 1.8,
        origin: 'Middle East'
    }
]

Product.insertMany(products, function (error) {
    console.error(">>>>>>>>>>>>>>>>>> dummyProducts insertMany Error = ", JSON.stringify(error))
    console.error(">>>>>>>>>>>>>>>>>> dummyProducts insert count = ", products.length)
    mongoose.connection.close()
    process.exit(0);
})
