const  {capitalise} = require("../../../models/models-helper");

describe('Returns capitalised string: ', () => {
    test('multiple whitespace at start, in-between and end.', () => {
        const actual = capitalise('   north   america  ')
        expect(actual).toBe('North America')
    })

    test('a mix of UPPER and lower case.', () => {
        const actual = capitalise('AfRiCa')
        expect(actual).toBe('Africa')
    })
})



