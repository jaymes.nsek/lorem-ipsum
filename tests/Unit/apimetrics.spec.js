const {ORIGIN_OPT_UNION, ALPHA_NUM_SYM_RE} = require("../../apimetrics");

test('Returns "origin" string union.', () => {
    expect(ORIGIN_OPT_UNION)
        .toBe('|Europe|South America|Africa|Australia|Middle East|North America')
})

test('Typical name/description passes ALPHA_NUM_SYM_RE constant.', () => {
    const str = 'Foster\'s Lager Beer Cans 4 x 440ml'
    // #match returns null if no match is found, else an array of matches
    expect(str.match(ALPHA_NUM_SYM_RE)).toBeTruthy()
})
