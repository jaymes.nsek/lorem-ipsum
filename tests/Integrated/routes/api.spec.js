process.env.NODE_ENV = 'test'
db = require('../../../db')
const req = require('supertest')
const app = require('../../../app')
const Product = require("../../../models/product");

let populatedDocs;

//Populate test database
beforeEach(async () => {
    const array = [
        ["Chicken Kyiv", "Chicken meal.", "3.99", "250g", "Europe"],
        ["Woodland Free Range Large Eggs", "Free range British eggs.", "2.20", "x12", "Europe"],
        ['Basmati Rice', 'Basmati White Rice.', '1.85', '1kg', 'Europe'],
        ['Mango', 'Sweet and delicious mango.', '0.65', '80g', 'South America']
    ]
    const productArr = array.map(arr => {
        return {name: arr[0], description: arr[1], price: arr[2], size: arr[3], origin: arr[4]}
    })

    populatedDocs = await new Promise(resolve => {
        Product.insertMany(productArr, (err, docs) => {
            resolve(docs)
        })
    })
})

/*test('true is true', () => {
    expect(true).toBe(true)
})*/


test('POST /api/product (covers GET /api/products): returns JSON with recently added doc id ' +
    '(and confirm data size incremented)',
    async () => {
        /** Main test **/
        let res = await req(app).post("/api/product")
            .send({
                name: 'Vitalite Dairy Free Cheese',
                description: 'Dairy Free alternative to Cheese with added calcium.',
                price: 2.00,
                size: '200g',
                origin: 'Europe'
            })

        // Added to DB
        expect(res.body.errors).toBeNull()
        expect(res.statusCode).toBe(201)
        // Check that _id is present and that all other props match those that were sent to backend
        expect(res.body.data).toHaveProperty('_id')
        expect(res.body.data).toMatchObject(
            {
                name: 'Vitalite Dairy Free Cheese',
                description: 'Dairy Free alternative to Cheese with added calcium.',
                price: 2.00,
                size: '200g',
                origin: 'Europe'
            }
        )

        /** Verification test **/
        // Number of products increased from 4 to 5
        res = await req(app).get('/api/products')
        expect(res.statusCode).toBe(200)
        expect(res.body.errors).toBeNull()
        expect(res.body.data.length).toBe(5)
    })

test('GET /api/product: returns with model corresponding to route id-param',
    async () => {
        /*** The first item in {@link populatedDocs} should be "Chicken Kyiv" ***/
        const res = await req(app).get('/api/product/' + populatedDocs[0]._id)
        expect(res).toHaveProperty(
            'body.data',
            {
                "__v": 0,
                "_id": populatedDocs[0]._id.toString(),
                "description": "Chicken meal.",
                "name": "Chicken Kyiv",
                "origin": "Europe",
                "price": 3.99,
                "size": "250g"
            }
        )

        expect(res.statusCode).toBe(200)
        expect(res.body.errors).toBeNull()
    })

test('UPDATE /api/product (covers GET /api/product/:id): ' +
    'updates Product associated with the _id',
    async () => {
        /** Main test **/
        let res = await req(app).put('/api/product/' + populatedDocs[0]._id)
            .send({
                name: 'Dairy Free Cheese',
                description: 'Dairy Free alternative to Cheese.',
                price: 2.49,
                size: '235g',
                origin: 'Australia'
            })

        expect(res.statusCode).toBe(201)
        expect(res.body.errors).toBeNull()
        expect(res.body.data).toMatchObject(
            {
                _id: populatedDocs[0]._id.toString(),
                name: 'Dairy Free Cheese',
                description: 'Dairy Free alternative to Cheese.',
                price: 2.49,
                size: '235g',
                origin: 'Australia'
            })

        /** Verification test **/
        // Verify with GET, that model reflects update params
        res = await req(app).get('/api/product/' + populatedDocs[0]._id)

        expect(res.statusCode).toBe(200)
        expect(res).toHaveProperty(
            'body.data',
            {
                "__v": 0,
                "_id": populatedDocs[0]._id.toString(),
                name: 'Dairy Free Cheese',
                description: 'Dairy Free alternative to Cheese.',
                price: 2.49,
                size: '235g',
                origin: 'Australia'
            }
        )
        expect(res.body.errors).toBeNull()
    })

describe('PARTIAL UPDATE /api/product : ', () => {
    test('updates Product and any missing value does not result in ValidationError.',
        async () => {
            let res = await req(app).put('/api/product/' + populatedDocs[0]._id)
                .send({
                    name: 'The Delicious Chicken Kyiv',
                    size: '500g',
                })

            expect(res.statusCode).toBe(201)
            expect(res.body.errors).toBeNull()
            expect(res.body.data).toMatchObject(
                {
                    "_id": populatedDocs[0]._id.toString(),
                    name: "The Delicious Chicken Kyiv", // Updated property
                    size: "500g", // Updated property
                    description: "Chicken meal.",
                    origin: "Europe",
                    price: 3.99
                })
        })

    test('Enum property (origin) is case-insensitive.', async () => {
        // Populated origin for element0 = "Europe", which is as specified on Product Mongoose model
        // Deviate from Product's "Africa" opt, and set 'AfRiCa'.
        let res = await req(app).put('/api/product/' + populatedDocs[0]._id)
            .send({
                origin: 'AfRiCa',
            })

        expect(res.body.errors).toBeNull()
        expect(res.body.data).toHaveProperty('origin', 'Africa')
        expect(res.statusCode).toBe(201)
    })
})


test('DELETE /api/product (covers GET /api/products): removes Product associated with the _id',
    async () => {
        // Assert that we start off with 4 items
        expect(populatedDocs.length).toBe(4)

        /** Main test **/
        let res = await req(app).delete('/api/product/' + populatedDocs[0]._id)
        expect(res.statusCode).toBe(200)
        expect(res.body.data).toBe(populatedDocs[0]._id.toString()) // The _id of the deleted row
        expect(res.body.data).not.toBeNull()

        /** Verification test **/
        // Verify with GET, that after delete, 3 items now exist
        res = await req(app).get('/api/products')
        expect(res.body.data.length).toBe(3)
    })

afterEach(async () => {
    await db.connection.dropCollection('products')
})

afterAll(async () => {
    await db.connection.close()
})
